<?php 
// Build out the Class to control the experience session
class quseExperience {

    public function __construct()
    {
        // echo 'experience: ' . $_SESSION['experience'];

            // If the experience is set AND doesn't equal the session then RETURN nothing
            if ( !empty($_SESSION['experience'])) { ?>

                <script>
                    var session = '<?php echo $_SESSION['experience'] ?>';

                    window.onload = function() {
                        //console.log(session);
                        $('.experience--' + session).removeClass('experience').addClass('experience--show');
                    }
                </script>

            <?php }
    }

    public function getExperience() 
    {
        if ( isset($_GET['experience']) ) {
            $_SESSION['experience'] = $_GET['experience']; ?>
            <script>
                var experience = <?php echo json_encode($_SESSION['experience'], JSON_HEX_TAG); ?>; // Don't forget the extra semicolon!
            </script>
    
        <?php } elseif (!empty($_SESSION['experience'])) { ?>
            <script>
                var experience = <?php echo json_encode($_SESSION['experience'], JSON_HEX_TAG); ?>; // Don't forget the extra semicolon!
            </script>
        <?php }
    }

    public function clearExperience()
    {
        // This function will be checking each of the blocks and showing/hiding them based on the experience set.
        if ( !empty($_SESSION['experience']) ) {
            unset($_SESSION['experience']);
        }
    }

}