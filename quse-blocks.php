<?php
/**
 * Plugin Name:     Quse Blocks
 * Plugin URI:      https://qusedevelopment.com
 * Description:     Quse Display Blocks
 * Author:          Quse Web Development
 * Author URI:      https://qusedevelopment.com
 * Text Domain:     quse-blocks
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Quse_Blocks
 */

include_once __DIR__ .'/blocks/quse-card.php';
include_once __DIR__ .'/blocks/quse-link.php';
include_once __DIR__ .'/blocks/quse-carousel.php';
include_once __DIR__ .'/blocks/quse-group.php';
include_once __DIR__ .'/blocks/quse-content.php';
include_once __DIR__ .'/blocks/quse-heading.php';
include_once __DIR__ .'/blocks/quse-slide.php';
include_once __DIR__ .'/blocks/quse-poster.php';
include_once __DIR__ .'/blocks/quse-display.php';
include_once __DIR__ .'/blocks/quse-nav.php';
include_once __DIR__ .'/blocks/quse-tabs.php';

// deregister default jQuery included with Wordpress
add_action( 'wp_enqueue_scripts', function(){

if (is_admin()) return;

    wp_deregister_script( 'jquery' );
    $jquery_cdn = 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js';
    wp_enqueue_script( 'jquery', $jquery_cdn, array(), '3.3.1', true );
});

/*
- Get the directories that need to be setup
- foreach through them and set each variable
- add function for backend enque
- add function for frontend 
*/

// This function adds a custom block category for my blocks
function quse_block_category( $categories, $post ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug' => 'quse-block-layouts',
				'title' => __( 'Quse Block Layouts', 'quse-block-layouts' ),
            ),
            array(
				'slug' => 'quse-block-components',
				'title' => __( 'Quse Block Components', 'quse-block-components' ),
			),
		)
	);
}
add_filter( 'block_categories', 'quse_block_category', 10, 2);

// Enque the style that has been compiled for each of the blocks
function quse_block_style_enqueue(){

    $style_path = dirname(realpath(__FILE__)).'/blocks/style/style.css';

    if ( file_exists( $style_path ) ) {
        wp_enqueue_style( 'quse-block-library-style', plugins_url('/blocks/style/style.css', __FILE__), false, '0.72' );
    }

}
add_action('init', 'quse_block_style_enqueue');
