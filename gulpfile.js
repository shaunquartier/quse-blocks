// Gulp Nodes   ``` npm install --no-bin-links ```
var gulp = require( 'gulp' ),
	plumber = require( 'gulp-plumber' ),
	sass = require( 'gulp-sass' ),
	autoprefixer = require('gulp-autoprefixer'),
	minifyCSS = require('gulp-clean-css'),
	watch = require( 'gulp-watch' ),
	globImporter = require('sass-glob-importer'),
	uglify = require( 'gulp-uglify' ),
	rename = require( 'gulp-rename' ),
	concat = require('gulp-concat');
	

// Error Handling
var onError = function( err ) {
	console.log( 'An error occurred:', err.message );
	this.emit( 'end' );
};

// sass
gulp.task('sass', function () {
  return gulp.src( './blocks/style/style.scss',  { sourcemap: true } )
  	.pipe(sass({ importer: globImporter() }))
    .pipe ( sass().on('error', sass.logError ) )
    .pipe( autoprefixer( 'last 4 version' ) )
    .pipe( minifyCSS( { keepBreaks: false,  } ) )
    .pipe( gulp.dest('./blocks/style/') );
});


// watch
gulp.task( 'watch', function() {

	// Watch all .scss files
    gulp.watch( ['./blocks/*/*.scss'], [ 'sass' ] );
    gulp.watch( ['./blocks/style/*.scss'], [ 'sass' ] );

});



// Default task -- runs scss and watch functions
gulp.task( 'default', ['sass', 'watch'] );
