/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { SelectControl, TextControl, Dashicon, BaseControl, PanelBody, PanelRow, Button } = wp.components;
const { InspectorControls, ColorPalette, InnerBlocks, MediaUpload, RichText } = wp.blockEditor;
const { AlignmentToolbar, BlockControls, BlockAlignmentToolbar } = wp.editor;

/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

/**
 * Base Components
 */
import BackgroundOptions, { BackgroundOptionsAttributes, BaseColors, BackgroundOptionsClasses, BackgroundOptionsInlineStyles, BackgroundOptionsVideoOutput, ExperienceClasses } from '../base';
import { HeadingType } from '../base/components';

export default registerBlockType ("quse-blocks/quse-display",
    {
        title: __("Display"),
        description: __("A block that generates a horizontal display"),
        category: "quse-block-layouts",
        icon: "analytics",
        attributes: {
            title: {
				type: "string",
				default: "Display Title"
            },
            titleSize: {
                type: "string",
                default: 'heading-two'
            },
            titleColor: {
                type: 'string'
            },
			text: {
                type: 'array',
                source: 'children',
                selector: '.display-text',
			},
            textColor: {
                type: 'string'
            },
            boxColor: {
                type: 'string',
                default: '#ffffff',
            },
            displayimgURL: {
				type: "string",
				source: "attribute",
				attribute: "src",
				selector: "img"
			},
			displayimgID: {
				type: "number"
			},
			displayimgAlt: {
				type: "string",
				source: "attribute",
				attribute: "alt",
				selector: "img"
            },
            style: {
                type: 'string',
                default: "row"
            },
            alignment: {
                type: 'string'
            },
            ...BackgroundOptionsAttributes,
        },
        
        edit: props => {
            
            const ALLOWED_BLOCKS = [ 'quse-blocks/quse-link' ];

            const styleOptions = [
                { label: 'Image on Left | Content on Right', value: 'row' },
                { label: 'Content on Left | Image on Right', value: 'row-reverse' },
            ];
            
            const ColorControl = (props) => (
                <BaseControl
                    label={props.label}
                >
                    <ColorPalette
                        colors={BaseColors}
                        value={props.value}
                        onChange={props.onChange}
                    />
                </BaseControl>
            );
    
            const onSelectImage = img =>
				props.setAttributes({
					displayimgID: img.id,
					displayimgURL: img.url,
					displayimgAlt: img.alt
				});
			
			const onRemoveImage = () =>
				props.setAttributes({
					displayimgID: null,
					displayimgURL: null,
					displayimgAlt: null
				});

            const onChangeAlignment = value => props.setAttributes({alignment: value});
            const onChangeStyle = value => props.setAttributes({style: value});
            const onChangeTitle = value => { props.setAttributes( { title: value } ) };
            const onChangeTitleSize = value => props.setAttributes({titleSize: value});
            const onChangeText = value => { props.setAttributes( { text: value } ) };
            const onChangeTitleColor = value => { props.setAttributes( { titleColor: value } ) };
            const onChangeBoxColor = value => { props.setAttributes( { boxColor: value } ) };
            const onChangeTextColor = value => { props.setAttributes( { textColor: value } ) };
            const onChangeBlockSize = value => props.setAttributes({blockAlignment: value});

            return (
                <div>
                    <div className="badge-block-name">Display Layout</div>
                    <BlockControls>
                        <BlockAlignmentToolbar
                            value={ props.attributes.blockAlignment }
                            onChange={ onChangeBlockSize }
                        />
                        <AlignmentToolbar
                            value={ props.attributes.alignment }
                            onChange={ onChangeAlignment }
                        />
                    </BlockControls>
                    <InspectorControls key="inspector">
                    { BackgroundOptions( props ) }
                    { !props.attributes.displayimgID ? (
                        <PanelBody
                            title={ __( 'Display Image' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <div className="media-upload-wrapper">
                                    <MediaUpload
                                        onSelect={onSelectImage}
                                        type="image"
                                        className="display_image"
                                        value={props.attributes.displayimgID}
                                        render={ ( { open } ) => (
                                            <Button className="button button-large" onClick={ open }>
                                                <Dashicon icon="format-image" /> { __( 'Upload Image' ) }
                                            </Button>
                                        ) }
                                    />
                                </div>
                            </PanelRow>
                        </PanelBody>
                    ) : (
                        <PanelBody
                            title={ __( 'Display Image' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                    <img
                                        src={ props.attributes.displayimgURL }
                                        alt={ props.attributes.displayimgAlt }
                                    />
                            </PanelRow>
                        </PanelBody>
                    ) }
                        <PanelBody
                            title={ __( 'Display Content' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Title Size' ) }
                                    value={props.attributes.titleSize}
                                    options={HeadingType}
                                    onChange={onChangeTitleSize}
                                />
                            </PanelRow>
                            <PanelRow>
                                <ColorControl
                                    label={'Title Color'}
                                    onChange={onChangeTitleColor}
                                    value={props.attributes.titleColor}
                                />
                            </PanelRow>
                            <PanelRow>
                                <ColorControl
                                    label={'Text Color'}
                                    onChange={onChangeTextColor}
                                    value={props.attributes.textColor}
                                />
                            </PanelRow>
                        </PanelBody>
                        <PanelBody
                            title={ __( 'Display Style' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Display Alignment' ) }
                                    value={props.attributes.style}
                                    options={styleOptions}
                                    onChange={onChangeStyle}
                                />
                            </PanelRow>
                            <PanelRow>
                                <ColorControl
                                    label={'Display Background Color'}
                                    onChange={onChangeBoxColor}
                                    value={props.attributes.boxColor}
                                />
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>
                    <section
                        className={ classnames(
                            'quse-block',
                            'display_box',
                            ...BackgroundOptionsClasses( props ),
                            ExperienceClasses(props),
                        ) }
                        style={ {
                            ...BackgroundOptionsInlineStyles( props ),
                        } }
                    >
                        <div className="background__overlay" style={{
                            backgroundColor: props.attributes.overlayColor,
                            opacity: props.attributes.overlayOpacity
                        }}></div>
                    { BackgroundOptionsVideoOutput( props ) }
                    
                    <div className={ classnames(
                        'display',
                        'container--' + props.attributes.blockAlignment,
                        'style--' + props.attributes.style,
                    )} style={{
                        flexDirection: props.attributes.style,
                    }}>
                            { ! props.attributes.displayimgID ? (
                                <div className="display-image-top">
                                    <MediaUpload
                                        onSelect={onSelectImage}
                                        type="image"
                                        className="display_image"
                                        value={props.attributes.displayimgID}
                                        render={ ( { open } ) => (
                                            <Button className="button button-large quse-media-button quse-media-button" onClick={ open }>
                                                <Dashicon icon="format-image" /> { __( 'Upload Image' ) }
                                            </Button>
                                        ) }
                                    />
                                </div>
                            ) : (
                                <div className="display-image-top">
                                    <img src={props.attributes.displayimgURL} alt={props.attributes.displayimgAlt} />

                                    <Button
                                        className="remove-image button button-large quse-media-button quse-media-button--overlay"
                                        onClick={ onRemoveImage }
                                    >
                                        <Dashicon icon="no-alt" /> { __( 'Remove Image' ) }
                                    </Button>
                                </div>
                            )}
                        <div className="display-body" style={{
                            'text-align': props.attributes.alignment,
                            backgroundColor: props.attributes.boxColor,
                        }}>
                            <div className="display-body__content">
                                <div className={ classnames(
                                        'display-title',
                                        props.attributes.titleSize,
                                    )}
                                    style={{
                                    color: props.attributes.titleColor
                                }}>
                                    <RichText
                                        value={ props.attributes.title } // Any existing content, either from the database or an attribute default
                                        allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                                        onChange={onChangeTitle} // Store updated content as a block attribute
                                    />
                                </div>
                                <div className="display-text" style={{
                                    color: props.attributes.textColor
                                }}>
                                    <RichText
                                        value={ props.attributes.text } // Any existing content, either from the database or an attribute default
                                        allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                                        onChange={onChangeText} // Store updated content as a block attribute
                                        placeholder={ __( 'Enter your text here...' ) } // Display this text before any content has been added by the user
                                    />
                                </div>
                                <InnerBlocks allowedBlocks={ ALLOWED_BLOCKS } />
                            </div>
                        </div>
                    </div>
                    </section>
                </div>
			);
		},

		save: props => {
            return (
                <section className={ classnames(
                    'quse-block',
                    'display_box',
                    ...BackgroundOptionsClasses( props ),
                    ExperienceClasses(props),
                ) } style={ {
                    ...BackgroundOptionsInlineStyles( props ),
                } }>
                
                    <div className="background__overlay" style={{
                        backgroundColor: props.attributes.overlayColor,
                        opacity: props.attributes.overlayOpacity
                    }}></div>

                    { BackgroundOptionsVideoOutput( props ) }

                    <div className={ classnames(
                        'display',
                        'container--' + props.attributes.blockAlignment,
                    )} style={{
                        flexDirection: props.attributes.style,
                    }}>

                        {props.attributes.displayimgURL &&
                            <div className="display-image-top">
                                <img src={props.attributes.displayimgURL} alt={props.attributes.displayimgAlt} />
                            </div>
                        }

                        <div className="display-body" style={{
                            'text-align': props.attributes.alignment,
                            backgroundColor: props.attributes.boxColor,
                        }}>

                            <div className="display-body__content">

                                <div className={ classnames(
                                    'display-title',
                                    props.attributes.titleSize,
                                )} style={{color: props.attributes.titleColor}}>
                                    {props.attributes.title}
                                </div>

                                <div className="display-text" style={{color: props.attributes.textColor}}>
                                    {props.attributes.text}
                                </div>

                                <InnerBlocks.Content />
                            </div> 
                        </div>
                    </div>
                </section>
            )
        }
	}
);
