import "./i18n";

/**
 * Import blocks
*/
import "./base";
import "./quse-author";
import "./quse-card";
import "./quse-carousel";
import "./quse-content";
import "./quse-display";
import "./quse-group";
import "./quse-grid-box";
import "./quse-heading";
import "./quse-image";
import "./quse-link";
import "./quse-modal";
import "./quse-nav";
import "./quse-post-list";
import "./quse-poster";
import "./quse-slide";
import "./quse-space";
import "./quse-tabs";
import "./quse-tab-content";
import "./quse-tab-item";
import "./quse-tab-link";
import "./quse-wp/";
