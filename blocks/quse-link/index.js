/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { SelectControl, TextControl, PanelBody, PanelRow, BaseControl } = wp.components;
const {InspectorControls, ColorPalette } = wp.blockEditor;
const { URLInput } = wp.editor;

/**
 * Base Components
 */
 import { BaseColors } from '../base';
import { LinkOptions, LinkStyle } from '../base/components';

export default registerBlockType ("quse-blocks/quse-link",
    {
        title: __("Link/Button"),
        description: __("A block that generates a link/button. Choose how you want the block to display in the settings below."),
        category: "quse-block-components",
        icon: "admin-links",
        parent: ['quse-blocks/quse-card'],
        attributes: {
            linkText: {
                type: 'string',
                source: 'text',
                selector: 'a',
                default: 'Read More'
            },
            link: {
                type: 'string',
                source: 'attribute',
                selector: 'a.nav-link',
                attribute: 'href'
            },
            linkTarget: {
                type: 'string',
                default: '_self',
            },
            linkClass: {
                type: 'string',
                default: 'nav-link quse__button quse__button--primary btn',
            },
            role: {
                type: 'string',
            },
            linkName: {
                type: 'string',
            },
            dataToggle: {
                type: 'string',
                default: null,
            },
            dataTarget: {
                type: 'string',
            },
            linkColor: {
                type: 'string',
            },
            linkTextColor: {
                type: 'string',
            },
            linkBorderRadius: {
                type: 'string',
                default: '0px',
            }
        },
        
        edit: props => {

            const onChangeLink = value => props.setAttributes({link: value});
            const onChangeLinkTarget = value => props.setAttributes({linkTarget: value});
            const onChangeLinkText = value => props.setAttributes({linkText: value});
            const onChangeLinkClass = value => props.setAttributes({linkClass: value});
            const onChangeDataTarget = value => props.setAttributes({dataTarget: value});
            const onChangeLinkColor = value => { props.setAttributes( { linkColor: value } ) };
            const onChangeLinkTextColor = value => { props.setAttributes( { linkTextColor: value } ) };
            const onChangeLinkBorderRadius = value => { props.setAttributes( { linkBorderRadius: value } ) };

            const ColorControl = (props) => (
                <BaseControl
                    label={props.label}
                >
                    <ColorPalette
                        colors={BaseColors}
                        value={props.value}
                        onChange={props.onChange}
                    />
                </BaseControl>
            );

            var myStr = props.attributes.linkText
            var modalTarget = props.attributes.link;

            if (props.attributes.linkText) {
                myStr = myStr.replace(/\s/g, "").toLowerCase();
            }

            if (props.attributes.linkClass == 'nav-link quse__tab link--primary') {
                props.setAttributes( { link: '#' + myStr } );
                props.setAttributes( { linkName: myStr + '-tab' } );
                props.setAttributes( { role: 'tab' } );
            } else {
                props.setAttributes( { linkName: myStr } );
                props.setAttributes( { role: 'button' } );
            }

            if (props.attributes.linkTarget == 'modal') {
                
                props.setAttributes( { dataToggle: 'modal' } );
                props.setAttributes( { dataTarget: '#' + modalTarget } );

            } else {
                props.setAttributes( { dataToggle: null } );
                props.setAttributes( { dataTarget: null } );
            }

            if (props.attributes.linkClass != 'nav-link quse__button quse__button--custom btn') {
                props.setAttributes( { linkColor: null } );
            }

            return (
                <div>
                    <InspectorControls key="inspector">
                        <PanelBody
                            title={ __( 'Link Settings' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Link Style' ) }
                                    value={props.attributes.linkClass}
                                    options={LinkStyle}
                                    onChange={onChangeLinkClass}
                                />
                            </PanelRow>
                            { props.attributes.linkClass == 'nav-link quse__button quse__button--custom btn' &&
                            <div>
                            <PanelRow>
                                <ColorControl
                                    label={'Link Color'}
                                    onChange={onChangeLinkColor}
                                    value={props.attributes.linkColor}
                                />
                            </PanelRow>
                            <PanelRow>
                                <ColorControl
                                    label={'Link Text Color'}
                                    onChange={onChangeLinkTextColor}
                                    value={props.attributes.linkTextColor}
                                />
                            </PanelRow>
                            <PanelRow>
                                <TextControl
                                    label={"Link Border Radius"}
                                    value={props.attributes.linkBorderRadius}
                                    onChange={onChangeLinkBorderRadius}
                                    help={'Default is 0px'}
                                />
                            </PanelRow>
                            </div>
                            }
                            <PanelRow>
                                <TextControl
                                    label={"Link Label"}
                                    value={props.attributes.linkText}
                                    onChange={onChangeLinkText}
                                />
                            </PanelRow>
                        </PanelBody>
                        </InspectorControls>
                            {props.attributes.linkClass != 'nav-link quse__tab link--primary' &&
                                <InspectorControls key="inspector">
                                    <PanelBody
                                        title={ __( 'Link Target' ) }
                                        className="quse-background-options"
                                        initialOpen={ false }
                                    >
                                        <PanelRow>
                                            <SelectControl
                                                label={ __( 'Link Type' ) }
                                                value={props.attributes.linkTarget}
                                                options={LinkOptions}
                                                onChange={onChangeLinkTarget}
                                            />
                                        </PanelRow>
                                        {props.attributes.linkTarget === 'modal' ? (
                                            <PanelRow>
                                                <TextControl
                                                    label={'Modal Destination'}
                                                    value={props.attributes.link}
                                                    help={"Enter the Modal Block ID here."}
                                                    onChange={onChangeLink}
                                                />
                                            </PanelRow>
                                        ):(
                                            <PanelRow>
                                                <URLInput
                                                    label={"Link"}
                                                    className="url-input"
                                                    value={ props.attributes.link }
                                                    onChange={onChangeLink}
                                                />
                                            </PanelRow>
                                        )}
                                    </PanelBody>
                                </InspectorControls>
                            }
                            
                    <a
                        style={{
                            backgroundColor: props.attributes.linkColor,
                            borderColor: props.attributes.linkColor,
                            color: props.attributes.linkTextColor,
                            borderRadius: props.attributes.linkBorderRadius,
                        }}
                        id={props.attributes.linkName}
                        className={props.attributes.linkClass}
                        target={props.attributes.linkTarget} 
                        rel="noopener noreferrer"
                        aria-pressed="true"
                        aria-controls={props.attributes.linkName}
                        role={props.attributes.role}
                        data-toggle={props.attributes.dataToggle}
                        data-target={props.attributes.dataTarget}
                    >
                        {props.attributes.linkText}
                    </a>
                </div>
			);
		},

		save: props => {

			return (

                <a 
                    style={{
                        backgroundColor: props.attributes.linkColor,
                        borderColor: props.attributes.linkColor,
                        color: props.attributes.linkTextColor,
                        borderRadius: props.attributes.linkBorderRadius,
                    }}
                    href={props.attributes.link}
                    id={props.attributes.linkName}
                    className={props.attributes.linkClass}
                    target={props.attributes.linkTarget} 
                    rel="noopener noreferrer"
                    aria-pressed="true"
                    aria-controls={props.attributes.linkName}
                    role={props.attributes.role}
                    data-toggle={props.attributes.dataToggle}
                    data-target={props.attributes.dataTarget}
                >
                    {props.attributes.linkText}
                </a>
			);
		}
	}
);
