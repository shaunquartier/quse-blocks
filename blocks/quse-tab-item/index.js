/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType, createBlock } = wp.blocks;
const {createElement} = wp.element;
const { TextControl, PanelBody, PanelRow } = wp.components;
const { InspectorControls, InnerBlocks } = wp.blockEditor;
const { dispatch } = wp.data;
const { AlignmentToolbar, BlockControls, BlockAlignmentToolbar } = wp.editor;

/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

export default registerBlockType ("quse-blocks/quse-tab-item",
    {
        title: __("Tab Content Group"),
        description: __("A block that generates items for a tab group."),
        category: "quse-block-components",
        parent: ['quse-blocks/quse-tabs'],
        icon: "text",
        attributes: {
            content: {
                type: 'string'
            },
            tabId: {
                type: 'string',
            },
        },
        
        edit: props => {

            const ALLOWED_BLOCKS = [ 'quse-blocks/quse-tab-content' ];
            const onChangeId = value => { props.setAttributes( { tabId: value } ) };

            return (
                <div>
                <InspectorControls key="inspector">
                        <PanelBody
                            title={ __( 'Tab Item Settings' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <TextControl
                                    label={"Tab Id (Optional)"}
                                    value={props.attributes.tabId}
                                    onChange={onChangeId}
                                />
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>
                    <div className={ classnames(
                        'tab-content',
                        'quse-tab-item'
                    )} id={props.attributes.tabId}>
                        <InnerBlocks 
                            allowedBlocks={ ALLOWED_BLOCKS } 
                        />
                    </div>
                </div>
			);
		},

		save: props => {
			return (
                <div className={ classnames(
                    'tab-content',
                    'quse-tab-item'
                )} id={props.attributes.tabId}>
                    <InnerBlocks.Content />
                </div>
			);
		}
	}
);
