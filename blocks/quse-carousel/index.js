/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { SelectControl, BaseControl, PanelBody, PanelRow } = wp.components;
const { InspectorControls, ColorPalette, InnerBlocks } = wp.blockEditor;
const { BlockControls, BlockAlignmentToolbar } = wp.editor;
/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.
//import "./base";

/**
 * Base Components
 */
import BackgroundOptions, { BackgroundOptionsAttributes, BaseColors, BackgroundOptionsClasses, BackgroundOptionsInlineStyles, BackgroundOptionsVideoOutput, ExperienceClasses } from '../base';

export default registerBlockType ("quse-blocks/quse-carousel",
    {
        title: __("Carousel Layout"),
        description: __("A block that generates a group of carousel items."),
        category: "quse-block-layouts",
        icon: "slides",
        attributes: {
            display: {
                type: 'string'
            },
            interval: {
                type: 'string'
            },
            arrows: {
                type: 'string',
                default: 'show'
            },
            ...BackgroundOptionsAttributes,
        },
        
        edit: props => {
            const ALLOWED_BLOCKS = [ 'core/image', 'quse-blocks/quse-slide' ];

            const intervalOptions = [
                { label: 'Slow (10 seconds)', value: '10000' },
                { label: 'Normal (7 seconds)', value: '7000' },
                { label: 'Fast (4 seconds)', value: '4000' },
            ];

            const arrowsOptions = [
                { label: 'Show', value: 'show' },
                { label: 'Hide', value: 'hide' },
            ];

            const onChangeInterval = value => props.setAttributes({interval: value});
            const onChangeArrows = value => props.setAttributes({arrows: value});
            const onChangeBlockSize = value => props.setAttributes({blockAlignment: value});

            return (
                <div>
                    <div className="badge-block-name">Carousel Layout</div>
                    <BlockControls>
                        <BlockAlignmentToolbar
                            value={ props.attributes.blockAlignment }
                            onChange={ onChangeBlockSize }
                        />
                    </BlockControls>
                    <InspectorControls key="inspector">
                        { BackgroundOptions( props ) }
                        <PanelBody
                            title={ __( 'Carousel Settings' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Show/Hide Arrows' ) }
                                    value={props.attributes.arrows}
                                    options={arrowsOptions}
                                    onChange={onChangeArrows}
                                />
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>
                    <section
                        className={ classnames(
                            'quse-block',
                            ...BackgroundOptionsClasses( props ),
                            ExperienceClasses(props),
                        ) }
                        style={ {
                            ...BackgroundOptionsInlineStyles( props ),
                        } }
                    >
                        <div className="background__overlay" style={{
                            backgroundColor: props.attributes.overlayColor,
                            opacity: props.attributes.overlayOpacity
                        }}></div>
                        { BackgroundOptionsVideoOutput( props ) }
                        <div className={ classnames(
                                'block-carousel-display',
                                'carousel',
                                'container--' + props.attributes.blockAlignment,
                            )} style={{ "justify-content": props.attributes.alignment }}>
                        <InnerBlocks allowedBlocks={ ALLOWED_BLOCKS } />
                        </div>
                    </section>
                </div>
			);
		},

		save: props => {
            return (
                <section
                    className={ classnames(
                    'quse-block',
                    ...BackgroundOptionsClasses( props ),
                    ExperienceClasses(props),
                ) }
                style={ {
                    ...BackgroundOptionsInlineStyles( props ),
                } }
                >

                <div className="background__overlay" style={{
                    backgroundColor: props.attributes.overlayColor,
                    opacity: props.attributes.overlayOpacity
                }}></div>

                { BackgroundOptionsVideoOutput( props ) }

                    <div className={ classnames(
                        'block-carousel-display',
                        'container--' + props.attributes.blockAlignment,
                    )} style={{ "justify-content": props.attributes.alignment }}>

                        <div id="quse-carousel" className="carousel slide carousel-fade" data-ride="carousel">

                            <div className="carousel__wrapper carousel-inner">
                                <InnerBlocks.Content />
                            </div>

                            { props.attributes.arrows == 'show' &&
                                <a className="carousel-control-prev" href="#quse-carousel" role="button" data-slide="prev">
                                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span className="sr-only">Previous</span>
                                </a>
                            }

                            { props.attributes.arrows == 'show' &&
                                <a className="carousel-control-next" href="#quse-carousel" role="button" data-slide="next">
                                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span className="sr-only">Next</span>
                                </a>
                            }

                        </div>
                    </div>
                </section>
            )
        }
	}
);
