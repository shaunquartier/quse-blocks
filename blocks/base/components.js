/**
 * Base Components
 */
export const MarginOptions = [
    { label: 'No Margin', value: '0' },
    { label: '1rem (16px) Top & Bottom', value: '1rem 0' },
    { label: '1rem (16px) Top Only', value: '1rem 0 0' },
    { label: '1rem (16px) Bottom Only', value: '0 0 1rem' },
    { label: '2rem (32px) Top & Bottom', value: '2rem 0' },
    { label: '2rem (32px) Top Only', value: '2rem 0 0' },
    { label: '2rem (32px) Bottom Only', value: '0 0 2rem' },
];

export const WrapperOptions = [
    { label: 'Full Width', value: 'full-width' },
    { label: 'Container Wrapper (1300px wide)', value: 'container-wrapper' },
    { label: 'Text Wrapper (800px wide)', value: 'text-wrapper' },
];

/**
 * Layout Components
 */
export const HeadingType = [
    {label: 'Heading 1', value: 'heading-one'},
    {label: 'Heading 2', value: 'heading-two'},
    {label: 'Heading 3', value: 'heading-three'},
    {label: 'Heading 4', value: 'heading-four'},
    {label: 'Heading 5', value: 'heading-five'},
    {label: 'Heading 6', value: 'heading-six'},
];

export const AlignmentOptions = [
    { label: 'Left Align', value: 'left' },
    { label: 'Center', value: 'center' },
    { label: 'Right Align', value: 'right' },
];

export const ImageSizeOptions = [
    { label: 'Cover', value: 'cover' },
    { label: 'Contain', value: 'contain' },
    { label: 'Auto', value: 'auto' },
];

export const ImagePositionOptions = [
    { label: 'Top Left', value: 'top left' },
    { label: 'Top Center', value: 'top center' },
    { label: 'Top Right', value: 'top right' },
    { label: 'Center Left', value: 'center left' },
    { label: 'Center', value: 'center' },
    { label: 'Center Right', value: 'center right' },
    { label: 'Bottom Left', value: 'bottom left' },
    { label: 'Bottom Center', value: 'bottom center' },
    { label: 'Bottom Right', value: 'bottom right' },
];

export const ImageAttachmentOptions = [
    { label: 'None', value: 'scroll' },
    { label: 'Fixed', value: 'fixed' },
];

export const LinkOptions = [
    { label: 'Current Tab', value: '_self' },
    { label: 'New Tab', value: '_blank' },
    { label: 'Open Modal', value: 'modal' },
];

export const LinkStyle = [
    { label: 'Primary Button', value: 'nav-link quse__button quse__button--primary btn' },
    { label: 'Secondary Button', value: 'nav-link quse__button quse__button--secondary btn' },
    { label: 'Clear Button', value: 'nav-link quse__button quse__button--clear btn' },
    { label: 'Custom Button', value: 'nav-link quse__button quse__button--custom btn' },
    { label: 'Hyperlink', value: 'nav-link quse__link link--primary' },
    { label: 'Tab', value: 'nav-link quse__tab link--primary' },
];

export const FlexOptions = [
    { label: 'Flex Left', value: 'flex-start' },
    { label: 'Center', value: 'center' },
    { label: 'Flex Right', value: 'flex-end' },
    { label: 'Space Between', value: 'space-between' },
    { label: 'Space Evenly', value: 'space-evenly' },
    { label: 'Space Around', value: 'space-around' },
];

export const GridStyleOptions = [
    { label: 'None', value: null },
    { label: 'Expand Outward', value: 'grid--expand' },
];

export const GridColumnOptions = [
    { label: '1 Wide', value: 'one-column' },
    { label: '2 Wide', value: 'two-columns' },
    { label: '3 Wide', value: 'three-columns' },
    { label: '4 Wide', value: 'four-columns' },
    { label: '5 Wide', value: 'five-columns' },
    { label: '6 Wide', value: 'six-columns' },
];

export const CaptionStyleOptions = [
    { label: 'Default (Clear Background)', value: 'style--default' },
    { label: 'Alternate (Background Color and Border)', value: 'style--alt' },
];

export const FlexOptionsHorizontal = [
    { label: 'Left', value: 'flex-start' },
    { label: 'Center', value: 'center' },
    { label: 'Right', value: 'flex-end' },
];

export const FlexOptionsVertical = [
    { label: 'Top', value: 'flex-start' },
    { label: 'Center', value: 'center' },
    { label: 'Bottom', value: 'flex-end' },
];