const BaseColors = [
    {name: 'Primary Color', color: '#a3bf61'},
    {name: 'Secondary Color', color: '#3a3a3c'},
    {name: 'White', color: '#ffffff'},
    {name: 'Black', color: '#000000'}
];

export default BaseColors;