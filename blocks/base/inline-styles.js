
function BackgroundOptionsInlineStyles( props ) {
    return {
        backgroundColor: 'color' === props.attributes.backgroundType ? props.attributes.backgroundColor : null,
        backgroundImage: 'image' === props.attributes.backgroundType && props.attributes.backgroundImage ? `url(${ props.attributes.backgroundImage.url })` : null,
        height: null !== props.attributes.imageHeight ? props.attributes.imageHeight + 'px' : null,
        backgroundSize: props.attributes.imageSize,
        backgroundPosition: props.attributes.imagePosition,
        margin: props.attributes.blockMargin,
        backgroundAttachment: props.attributes.imageAttachment,
    };
}

export default BackgroundOptionsInlineStyles;