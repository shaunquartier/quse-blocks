function ExperienceClasses( props ) {

    // If the layout has an experience set
    if ( props.attributes.experience ) {

        // Create a class string to be set
        const experienceString = 'experience--' + props.attributes.experience;
        const setString = experienceString.replace(/ /g, '')
        const experienceClass = 'experience ' + setString;
        return experienceClass;

    } else {
        return '';
    }

}

export default ExperienceClasses;