const BackgroundOptionsAttributes = {
    backgroundType: {
        type: 'string',
    },
    backgroundImage: {
        type: 'object',
    },
    imageHeight: {
        type: 'string',
    },
    imageSize: {
        type: 'string',
        default: 'cover',
    },
    imagePosition: {
        type: 'string',
        default: 'center',
    },
    imageAttachment: {
        type: 'string',
        default: 'scroll',
    },
    backgroundVideo: {
        type: 'object',
    },
    backgroundColor: {
        type: 'string',
    },
    wrapperWidth: {
        type: 'string',
    },
    blockAlignment: {
        type: 'string',
        default: 'center',
    },
    blockId: {
        type: 'string',
    },
    blockMargin: {
        type: 'string',
    },
    overlayColor: {
        type: 'string',
    },
    overlayOpacity: {
        type: 'integer',
    },
    experience: {
        type: 'string',
    },
    urlParam: {
        type: 'string',
    }
};

export default BackgroundOptionsAttributes;