/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { TextControl, BaseControl, SelectControl, Button, Dashicon, PanelBody, PanelColor, PanelRow, RangeControl } = wp.components;
const {InspectorControls, ColorPalette, MediaUpload, description, InnerBlocks } = wp.blockEditor;
const { withState } = wp.compose;
const { addFilter } = wp.hooks;
/**
 * Internal dependencies
 */

import BackgroundOptionsAttributes from './attributes';
import BaseColors from './colors';
import BackgroundOptionsClasses from './classes';
import ExperienceClasses from './experience';
import BackgroundOptionsInlineStyles from './inline-styles';
import BackgroundOptionsVideoOutput from './video';
import {
    HeadingType,
    MarginOptions,
    ImageSizeOptions, 
    ImagePositionOptions,
    ImageAttachmentOptions,
} from "./components";

// Export for ease of importing in individual blocks.
export {
    BackgroundOptionsAttributes,
    BaseColors,
    HeadingType,
    BackgroundOptionsClasses,
    ExperienceClasses,
    BackgroundOptionsInlineStyles,
    BackgroundOptionsVideoOutput,
};

addFilter(
    'blocks.registerBlockType',
    'quse-blocks/quse-set-block-width',
    modifyDefaultAlignment,
);

// Adding a filter to set the Block Width
function modifyDefaultAlignment(settings) {

        // Check for block type
    const newSettings = {
        ...settings,
        attributes: {
            ...settings.attributes,
            blockAlignment: { type: 'string', default: 'center' },
        },
        getEditWrapperProps( { blockAlignment } ) {
            if ( 'left' === blockAlignment || 'right' === blockAlignment || 'full' === blockAlignment || 'wide' === blockAlignment ) {
                return { 'data-align': blockAlignment };
            }
        },
    };
    return newSettings;
}

function BackgroundOptions( props ) {

    const setBackgroundType = value => props.setAttributes( { backgroundType: value } );
    const setBackgroundImage = value => props.setAttributes( { backgroundImage: value } );
    const removeBackgroundImage = () => props.setAttributes( { backgroundImage: null } );
    const onChangeImageHeight = value => props.setAttributes({imageHeight: value});
    const onChangeImageSize = value => props.setAttributes({imageSize: value});
    const onChangeImagePosition = value => props.setAttributes({imagePosition: value});
    const onChangeImageAttachment = value => props.setAttributes({imageAttachment: value});
    const setBackgroundVideo = value => props.setAttributes( { backgroundVideo: value } );
    const removeBackgroundVideo = () => props.setAttributes( { backgroundVideo: null } );
    const setBackgroundColor = value => props.setAttributes( { backgroundColor: value } );
    const setOverlayColor = value => props.setAttributes( { overlayColor: value } );
    const onChangeOpacity = value => props.setAttributes( { overlayOpacity: value } );
    const onChangeId = value => props.setAttributes({blockId: value});
    const onChangeMargin = value => props.setAttributes({blockMargin: value});
    const onChangeExperience = value => props.setAttributes({experience: value});

    const ColorControl = (props) => (
        <BaseControl
            label={props.label}
        >
            <ColorPalette
                colors={BaseColors}
                value={props.value}
                onChange={props.onChange}
            />
        </BaseControl>
    );

    const blockControl = () => {
        return (
            <BlockControls>
                <BlockAlignmentToolbar
                    value={ blockAlignment }
                    onChange={ blockAlignment => setAttributes( { blockAlignment } ) }
                />
                <AlignmentToolbar
                    value={ textAlignment }
                    onChange={ textAlignment => setAttributes( { textAlignment } ) }
                />
            </BlockControls>
        );
    };

    const baseOptionsSelect = () => {

        return (
            <div>
                <TextControl
                    label={"Block ID (Optional)"}
                    value={props.attributes.blockId}
                    onChange={onChangeId}
                />

                <SelectControl
                    label={ __( 'Block Margin' ) }
                    value={props.attributes.blockMargin}
                    options={MarginOptions}
                    onChange={onChangeMargin}
                    help={"This will set the margin above and below the block. "}
                />
            </div>
        );
    };

    const experienceOptionsSelect = () => {

        return (
            <TextControl
                label={"Set Block Experience"}
                value={props.attributes.experience}
                onChange={onChangeExperience}
            />
        );
    };

    const imageBackgroundSelect = () => {

        if ( 'image' !== props.attributes.backgroundType ) {
			return '';
        }

		if ( ! props.attributes.backgroundImage ) {
			return (
				<div className="media-upload-wrapper">
					<p>
						<MediaUpload
							buttonProps={ {
								className: 'components-button button button-large',
							} }
							onSelect={ setBackgroundImage }
							type="image"
							value=""
							render={ ( { open } ) => (
								<Button className="button button-large" onClick={ open }>
									<Dashicon icon="format-image" /> { __( 'Upload Image' ) }
								</Button>
							) }
						/>
					</p>
				</div>
			);
        }
        
        return (
            <div className="image-wrapper">
                <p>
                    <img
                        src={ props.attributes.backgroundImage.url }
                        alt={ props.attributes.backgroundImage.alt }
                    />
                </p>
                    <div className="media-button-wrapper">
                        <p>
                            <Button
                                className="remove-image button button-large"
                                onClick={ removeBackgroundImage }
                            >
                                <Dashicon icon="no-alt" /> { __( 'Remove Image' ) }
                            </Button>
                        </p>
                    </div>
                    <div className="image-properties">
                        <h3>Additional Image Settings</h3>
                        <TextControl
                            label={"Set Min Height: "}
                            className={"small-box"}
                            value={props.attributes.imageHeight}
                            onChange={onChangeImageHeight}
                        />
                        <SelectControl
                            label={ __( 'Image Size: ' ) }
                            className={"small-box"}
                            value={props.attributes.imageSize}
                            options={ImageSizeOptions}
                            onChange={onChangeImageSize}
                        />
                        <SelectControl
                            label={ __( 'Image Position: ' ) }
                            className={"small-box"}
                            value={props.attributes.imagePosition}
                            options={ImagePositionOptions}
                            onChange={onChangeImagePosition}
                        />
                        <SelectControl
                            label={ __( 'Image Attachment: ' ) }
                            className={"small-box"}
                            value={props.attributes.imageAttachment}
                            options={ImageAttachmentOptions}
                            onChange={onChangeImageAttachment}
                        />
                    </div>
            </div>
        ); // End of the return
        
    }; // End of BackgroundImage

    const videoBackgroundSelect = () => {
		if ( 'video' !== props.attributes.backgroundType ) {
			return '';
		}

		if ( ! props.attributes.backgroundVideo ) {
			return (
				<div className="media-upload-wrapper">
					<p>
						<MediaUpload
							buttonProps={ {
								className: 'components-button button button-large',
							} }
							onSelect={ setBackgroundVideo }
							type="video"
							value=""
							render={ ( { open } ) => (
								<Button className="button button-large" onClick={ open }>
									<Dashicon icon="format-video" /> { __( 'Upload Video' ) }
								</Button>
							) }
						/>
					</p>
					<p>
						<description>
							{ __( 'Add/Upload a 1920x1080 .mp4 video file. Note: background videos are only supported on heroes.' ) }
						</description>
					</p>
				</div>
			);
		}

		return (
			<div className="video-wrapper">
				<p>
					<video className="video-container video-container-overlay">
						<source
							type="video/mp4"
							src={ props.attributes.backgroundVideo.url }
						/>
					</video>
				</p>
				{ props.focus ? (
					<div className="media-button-wrapper">
						<p>
							<Button
								className="remove-video button button-large"
								onClick={ removeBackgroundVideo }
							>
								<Dashicon icon="no-alt" /> { __( 'Remove Video' ) }
							</Button>
						</p>

						<p>
							<description>
								{ __( 'Add/Upload a 1920x1080 .mp4 video file. Note: background videos are only supported on heroes.' ) }
							</description>
						</p>
					</div>
				) : null }
			</div>
		);
    }; // End of BackgroundVideo
    
    const colorPanelSelect = () => {
		if ( 'color' !== props.attributes.backgroundType ) {
			return '';
        }

		return (
				<ColorControl
                    label={'Background Color'}
                    onChange={setBackgroundColor}
                    value={props.attributes.backgroundColor}
                />
		);
    }; // End of BackgroundColor

    const backgroundOverlaySelect = () => {

        if ( 'none' == props.attributes.backgroundType ) {
			return '';
        }

        if ( null == props.attributes.backgroundType ) {
			return '';
        }
            
        return (
            <div>
                <ColorControl
                    label={'Overlay Color'}
                    onChange={setOverlayColor}
                    value={props.attributes.overlayColor}
                />
                
                <RangeControl
                    label="Overlay Opacity"
                    value={ props.attributes.overlayOpacity }
                    help={"Must be between 0 - 1"}
                    onChange={ onChangeOpacity }
                    min={ 0 }
                    max={ 1 }
                    step={ 0.05 }
                />
            </div>
        );
    };
    
    return (
        <div>
        <PanelBody
            title={ __( 'Block Settings' ) }
            className="quse-background-options"
            initialOpen={ false }
        >
            <PanelRow>
				{ baseOptionsSelect() }
			</PanelRow>
        </PanelBody>
        <PanelBody
            title={ __( 'Experience Settings' ) }
            className="quse-background-options"
            initialOpen={ false }
        >
            <PanelRow>
				{ experienceOptionsSelect() }
			</PanelRow>
        </PanelBody>
		<PanelBody
			title={ __( 'Block Background' ) }
			className="quse-background-options"
			initialOpen={ false }
		>
            <p>This background will be set to the full width of the screen behind your block.</p>
			<PanelRow>
				<SelectControl
					key="background-type"
					label={ __( 'Background Type' ) }
					value={ props.attributes.backgroundType ? props.attributes.backgroundType : '' }
					options={ [
						{
							label: __( 'None' ),
							value: '',
						},
						{
							label: __( 'Image' ),
							value: 'image',
						},
						{
							label: __( 'Video' ),
							value: 'video',
						},
						{
							label: __( 'Color' ),
							value: 'color',
						},
					] }
					onChange={ setBackgroundType }
				/>
			</PanelRow>
			<PanelRow>
				{ imageBackgroundSelect() }
				{ videoBackgroundSelect() }
				{ colorPanelSelect() }
			</PanelRow>
            <PanelRow>
                { backgroundOverlaySelect() }
            </PanelRow>
		</PanelBody>
        </div>
	);

}

export default BackgroundOptions;
