/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { InspectorControls, InnerBlocks } = wp.blockEditor;
const { BlockControls, BlockAlignmentToolbar } = wp.editor;

/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

/**
 * Base Components
 */
import BackgroundOptions, { BackgroundOptionsAttributes, BackgroundOptionsClasses, BackgroundOptionsInlineStyles, BackgroundOptionsVideoOutput, ExperienceClasses } from '../base';

export default registerBlockType ("quse-blocks/quse-wp",
    {
        title: __("Add WordPress Block(s)"),
        description: __("A block that will add a WordPress block within our template."),
        category: "quse-block-layouts",
        icon: "tickets",
        attributes: {
            embedUrl: {
                type: 'string',
            },
            ...BackgroundOptionsAttributes,
        },
        
        edit: props => {

            // const onChangeEmbedUrl = value => { props.setAttributes( { embedUrl: value } ) };
            const onChangeBlockSize = value => props.setAttributes({blockAlignment: value});
        
            return (
                <section className={ classnames(
                    'quse-block',
                    ...BackgroundOptionsClasses( props ),
                    ExperienceClasses(props),
                ) } style={ {
                    ...BackgroundOptionsInlineStyles( props ),
                } }>
                    <div className="badge-block-name">Add WordPress Blocks</div>
                    <BlockControls>
                        <BlockAlignmentToolbar
                            value={ props.attributes.blockAlignment }
                            onChange={ onChangeBlockSize }
                        />
                    </BlockControls>
                    <InspectorControls key="inspector">
                        { BackgroundOptions( props ) }
                    </InspectorControls>
                    <div className="background__overlay" style={{
                        backgroundColor: props.attributes.overlayColor,
                        opacity: props.attributes.overlayOpacity
                    }}></div>
                    { BackgroundOptionsVideoOutput( props ) }
                        <div className={ classnames(
                            'add__wordpress__block',
                            'container-wrapper',
                        )}>
                            <InnerBlocks />
                        </div>
                </section>
            )
		},

		save: props => {
            return (
                <section className={ classnames(
                    'quse-block',
                    ...BackgroundOptionsClasses( props ),
                    ExperienceClasses(props),
                ) } style={ {
                    ...BackgroundOptionsInlineStyles( props ),
                } }>

                    <div className="background__overlay" style={{
                        backgroundColor: props.attributes.overlayColor,
                        opacity: props.attributes.overlayOpacity
                    }}></div>

                    { BackgroundOptionsVideoOutput( props ) }

                    <div className={ classnames(
                        'add__wordpress__block',
                        'container-wrapper',
                    )}>
                        
                        <InnerBlocks.Content />
                    </div>
                </section>
            )
        },
        
	}
);
