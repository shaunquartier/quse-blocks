/**
 * WordPress dependencies
 */
 const { __ } = wp.i18n;
 const { registerBlockType } = wp.blocks;
 const {createElement} = wp.element;
 const { SelectControl, TextControl, Dashicon, BaseControl, PanelBody, PanelRow, Button, RangeControl } = wp.components;
 const { InspectorControls, ColorPalette, InnerBlocks, MediaUpload, RichText } = wp.blockEditor;
 const { AlignmentToolbar, BlockControls } = wp.editor;

 /**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

import BackgroundOptions, { BackgroundOptionsAttributes, BaseColors, BackgroundOptionsClasses, BackgroundOptionsInlineStyles, BackgroundOptionsVideoOutput, ExperienceClasses } from '../base';
import { FlexOptions, GridColumnOptions, GridStyleOptions } from '../base/components';


export default registerBlockType("quse-blocks/quse-grid-box",
    {
        title: __("Box"),
        description: __("A Clickable box block that contains a background image with some text."),
        category: "quse-block-layouts",
        icon: "format-aside",
        parent: ['quse-blocks/quse-group'],
        attributes: {
            boxTitle: {
                type: "string",

            },
			boximgID: {
				type: "number"
			}, 
            boximgURL: {
				type: "string",
			},
			boximgAlt: {
				type: "string",
				source: "attribute",
				attribute: "alt",
				selector: "img"
            },
            boximgSize: {
                type: 'string',
                default: 'cover',
            },
            boximgPosition: {
                type: 'string',
                default: 'center'
            },
            boxURL: {
                type: "string"
            }
        },
        edit: props =>{

            const onSelectImage = img =>
				props.setAttributes({
					boximgID: img.id,
					boximgURL: img.url,
					boximgAlt: img.alt
                });

            const onRemoveImage = () =>
				props.setAttributes({
					boximgID: null,
					boximgURL: null,
					boximgAlt: null
            });

            const onChangeTitle = (value) => {
                props.setAttributes({
                    boxTitle: value
                })
            }

            const onChangeURL = (value) => {
                return props.setAttributes({
                    boxURL: value
                });
            }

            return (
                <div className = "grid-box">
                    <InspectorControls key = "inspector">
                        <PanelBody title = { __( 'Grid Box Image' ) } className = "quse-background-options" initialOpen = {false}>
                            <PanelRow>
                                <img src = {props.attributes.boximgURL} />
                            </PanelRow>
                            <PanelRow>
                                {!props.attributes.boximgID ? 
                                    (
                                    <div className="media-upload-wrapper">
                                        <MediaUpload
                                            onSelect={onSelectImage}
                                            type="image"
                                            className="card_image"
                                            value={props.attributes.cardimgID}
                                            render={ ( { open } ) => (
                                                <Button className="button button-large" onClick={ open }>
                                                    <Dashicon icon="format-image" /> { __( 'Upload Image' ) }
                                                </Button>
                                            ) }
                                        />
                                    </div>
                                    )
                                    :
                                    (
                                    <Button
                                        className="remove-image button button-large quse-media-button quse-media-button--overlay"
                                        onClick={ onRemoveImage }
                                    >
                                        <Dashicon icon="no-alt" /> { __( 'Remove Image' ) }
                                    </Button>
                                    )
                                }
                                
                            </PanelRow>
                        </PanelBody>
                        <PanelBody title = { __( 'Grid Box URL' ) } className = "quse-background-options" initialOpen = {false}>
                            <TextControl
                                label={__("Grid Box URL")}
                                value={props.attributes.boxURL}
                                onChange={onChangeURL}
                            />
                        </PanelBody>
                        <PanelBody title = { __( 'Grid Box Title' ) } className = "quse-background-options" initialOpen = {false}>
                            <TextControl
                                label={__("Grid Box Title")}
                                value={props.attributes.boxTitle}
                                onChange={onChangeTitle}
                            />
                        </PanelBody>
                    </InspectorControls>
                    {!props.attributes.boximgID ? (
                        <div className = "box">
                            <MediaUpload
                                onSelect={onSelectImage}
                                type="image"
                                className="card_image"
                                value={props.attributes.boximgID}
                                render={ ( { open } ) => (
                                    <Button className="button button-large quse-media-button quse-media-button" onClick={ open }>
                                        <Dashicon icon="format-image" /> { __( 'Upload Image' ) }
                                    </Button>
                                )}
                            />
                        </div>
                    ) : (
                        <div className = "box">

                            <Button
                                className="remove-image button button-large quse-media-button quse-media-button--overlay"
                                onClick={ onRemoveImage }
                            >
                                <Dashicon icon="no-alt" /> { __( 'Remove Image' ) }
                            </Button>
                            
                            <span className = "box-text-descriptor">
                                <div className = "title-info">
                                    <RichText
                                        value={ props.attributes.boxTitle } // Any existing content, either from the database or an attribute default
                                        allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                                        onChange={onChangeTitle} // Store updated content as a block attribute
                                        placeholder={ __( 'Enter preview text here...' ) } // Display this text before any content has been added by the user
                                    />
                                </div>
                            </span>
                            <img className = "box-image" src = {props.attributes.boximgURL}></img>
                        </div>
                    )
                    }

                </div>
            )
        },
        save: props =>{
            return (
                <div className = "grid-box">

                        <div className = "box">
                            <a href = {props.attributes.boxURL}>
                                <span className = "box-text-descriptor">
                                    <div className = "title-info">
                                        {props.attributes.boxTitle}
                                    </div>
                                </span>
                                <img className = "box-image" src = {props.attributes.boximgURL}></img>
                            </a>
                        </div>
                    
                </div>
            );
        }
    }
)