/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { InspectorControls, InnerBlocks } = wp.blockEditor;
const { BlockControls, BlockAlignmentToolbar } = wp.editor;

/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

/**
 * Base Components
 */
import BackgroundOptions, { BackgroundOptionsAttributes, BaseColors, BackgroundOptionsClasses, BackgroundOptionsInlineStyles, BackgroundOptionsVideoOutput, ExperienceClasses } from '../base';

export default registerBlockType ("quse-blocks/quse-tabs",
    {
        title: __("Tabs Layout"),
        description: __("A block that generates a tab group."),
        category: "quse-block-layouts",
        attributes: {
            ...BackgroundOptionsAttributes,
        },
        
        edit: props => {

            const TEMPLATE = [
                ['quse-blocks/quse-nav', { style: 'nav nav-tabs', function: 'tabs' }, [
                    ['quse-blocks/quse-tab-link', {}, []]
                ]],
                ['quse-blocks/quse-tab-item', {}, [
                    ['quse-blocks/quse-tab-content', { blockid: 'tablabel', content: 'Enter the content for your tab here.' }, []]
                ]],
            ]

            const onChangeBlockSize = value => props.setAttributes({blockAlignment: value});

            return (
                <section
                    className={ classnames(
                        'quse-block',
                        ...BackgroundOptionsClasses( props ),
                        ExperienceClasses(props),
                    ) }
                    style={ {
                        ...BackgroundOptionsInlineStyles( props ),
                    } }
                >
                    <div className="badge-block-name">Tabs Layout</div>
                    <BlockControls>
                        <BlockAlignmentToolbar
                            value={ props.attributes.blockAlignment }
                            onChange={ onChangeBlockSize }
                        />
                    </BlockControls>
                    <InspectorControls key="inspector">
                        { BackgroundOptions( props ) }
                    </InspectorControls>
                    
                    <div className="background__overlay" style={{
                        backgroundColor: props.attributes.overlayColor,
                        opacity: props.attributes.overlayOpacity
                    }}></div>
                    <div className={ classnames(
                        'quse-tabs',
                        props.attributes.wrapperWidth,
                    )}>
                    { BackgroundOptionsVideoOutput( props ) }
                        <div class="quse-tabs__inner">
                            <InnerBlocks template={TEMPLATE} />
                        </div>
                    </div>
                </section>
			);
		},

		save: props => {
            return (
                <section className={ classnames(
                    'quse-tabs',
                    ...BackgroundOptionsClasses( props ),
                    'quse-block',
                    'container-wrapper',
                    ExperienceClasses(props),
                )} style={ {
                    ...BackgroundOptionsInlineStyles( props ),
                } }>

                    <div className="background__overlay" style={{
                        backgroundColor: props.attributes.overlayColor,
                        opacity: props.attributes.overlayOpacity
                    }}></div>

                    { BackgroundOptionsVideoOutput( props ) }

                    <div class="quse-tabs__inner">
                        <InnerBlocks.Content />
                    </div>

                </section>
            )
        },
        
	}
);
