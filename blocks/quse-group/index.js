/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { PanelBody, PanelRow, SelectControl } = wp.components;
const { InspectorControls, InnerBlocks } = wp.blockEditor;
const { BlockControls, BlockAlignmentToolbar } = wp.editor;

/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

/**
 * Base Components
 */
import BackgroundOptions, { BackgroundOptionsAttributes, BaseColors, BackgroundOptionsClasses, BackgroundOptionsInlineStyles, BackgroundOptionsVideoOutput, ExperienceClasses } from '../base';
import { FlexOptions, GridColumnOptions, GridStyleOptions } from '../base/components';

export default registerBlockType ("quse-blocks/quse-group",
    {
        title: __("Grid Layout"),
        description: __("A block that generates a group of items in a grid layout. Choose how you want the blocks to display in the settings below."),
        category: "quse-block-layouts",
        icon: "screenoptions",
        attributes: {
            alignment: {
                type: 'string',
                default: 'flex-start',
            },
            template: {
                type: 'string'
            },
            style: {
                type: 'string',
                default: null,
            },
            ...BackgroundOptionsAttributes,
        },
        
        edit: props => {
            const ALLOWED_BLOCKS = [ 'core/image', 'quse-blocks/quse-content', 'quse-blocks/quse-card', 'quse-blocks/quse-grid-box' ];
            const onChangeAlignment = value => { props.setAttributes( { alignment: value } ) };
            const onChangeTemplate = value => { props.setAttributes( { template: value } ) };
            const onChangeStyle = value => { props.setAttributes( { style: value } ) };
            const onChangeBlockSize = value => props.setAttributes({blockAlignment: value});

            return (
                <div>
                    <div className="badge-block-name">Grid Layout</div>
                    <BlockControls>
                        <BlockAlignmentToolbar
                            value={ props.attributes.blockAlignment }
                            onChange={ onChangeBlockSize }
                        />
                    </BlockControls>
                    <InspectorControls key="inspector">
                        { BackgroundOptions( props ) }
                        <PanelBody
                            title={ __( 'Grid Settings' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Alignment: ' ) }
                                    value={props.attributes.alignment}
                                    options={FlexOptions}
                                    onChange={onChangeAlignment}
                                />
                            </PanelRow>
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Layout: ' ) }
                                    value={props.attributes.template}
                                    options={GridColumnOptions}
                                    onChange={onChangeTemplate}
                                />
                            </PanelRow>
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Style: ' ) }
                                    value={props.attributes.style}
                                    options={GridStyleOptions}
                                    onChange={onChangeStyle}
                                />
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>
                    <section
                        className={ classnames(
                            'quse-block',
                            ...BackgroundOptionsClasses( props ),
                            ExperienceClasses(props),
                        ) }
                        style={ {
                            ...BackgroundOptionsInlineStyles( props ),
                        } }
                    >
                        <div className="background__overlay" style={{
                            backgroundColor: props.attributes.overlayColor,
                            opacity: props.attributes.overlayOpacity
                        }}></div>
                        { BackgroundOptionsVideoOutput( props ) }
                        <div className={ classnames(
                                'grid ' + props.attributes.style,
                                props.attributes.template,
                                'container--' + props.attributes.blockAlignment,
                                'grid-' + props.attributes.alignment,
                            )} style={{ "justify-content": props.attributes.alignment }}>
                                <div className="grid__inner">
                                    <InnerBlocks allowedBlocks={ ALLOWED_BLOCKS } />
                                </div>
                        </div>
                    </section>
                </div>
			)
		},

		save: props => {
            return (
                <section className={ classnames(
                    'quse-block',
                    ...BackgroundOptionsClasses( props ),
                    ExperienceClasses(props),
                ) } style={ {
                    ...BackgroundOptionsInlineStyles( props ),
                } }>
                
                    <div className="background__overlay" style={{
                        backgroundColor: props.attributes.overlayColor,
                        opacity: props.attributes.overlayOpacity
                    }}></div>
                    
                    { BackgroundOptionsVideoOutput( props ) }
                    
                    <div className={ classnames(
                        'grid',
                        props.attributes.template,
                        'container--' + props.attributes.blockAlignment,
                    )}>
                        <div className={ classnames(
                            'grid__inner',
                            props.attributes.template,
                        )} style={{ "justify-content": props.attributes.alignment }}>
                            <InnerBlocks.Content />
                        </div>
                    </div>
                </section>
            )
        },
        
	}
);
