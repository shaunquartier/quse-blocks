/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { TextControl, PanelBody, PanelRow } = wp.components;
const { InspectorControls } = wp.blockEditor;
const { BlockControls, BlockAlignmentToolbar } = wp.editor;

/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

/**
 * Base Components
 */
import BackgroundOptions, { BackgroundOptionsAttributes, BackgroundOptionsClasses, BackgroundOptionsInlineStyles, BackgroundOptionsVideoOutput } from '../base';

export default registerBlockType ("quse-blocks/quse-space",
    {
        title: __("Add Space"),
        description: __("A block that displays extra space for your page."),
        category: "quse-block-layouts",
        icon: "align-wide",
        attributes: {
            space: {
                type: 'string',
                default: '40px',
            },
            ...BackgroundOptionsAttributes,
        },
        
        edit: props => {

            const onChangeSpace = value => { props.setAttributes( { space: value } ) };
            const onChangeBlockSize = value => props.setAttributes({blockAlignment: value});
        
            return (
                <div>
                    <div className="badge-block-name">Space</div>
                    <BlockControls>
                        <BlockAlignmentToolbar
                            value={ props.attributes.blockAlignment }
                            onChange={ onChangeBlockSize }
                        />
                    </BlockControls>
                    <InspectorControls key="inspector">
                        { BackgroundOptions( props ) }
                        <PanelBody
                            title={ __( 'Space Options' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <TextControl
                                    label={"Space Height"}
                                    value={props.attributes.space}
                                    onChange={onChangeSpace}
                                />
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>
                    <section
                        className={ classnames(
                            props.className,
                            ...BackgroundOptionsClasses( props ),
                        ) }
                        style={ {
                            ...BackgroundOptionsInlineStyles( props ),
                        } }
                    >
                        <div className="background__overlay" style={{
                            backgroundColor: props.attributes.overlayColor,
                            opacity: props.attributes.overlayOpacity
                        }}></div>
                    { BackgroundOptionsVideoOutput( props ) }
                            <div className={ classnames(
                                'space',
                                props.attributes.wrapperWidth,
                            )} style={{height: props.attributes.space}}>
                            </div>
                    </section>

                </div>
                )
		},

		save: props => {
			return (
                    <section
                        className={ classnames(
                            props.className,
                            ...BackgroundOptionsClasses( props ),
                        ) }
                        style={ {
                            ...BackgroundOptionsInlineStyles( props ),
                        } }
                    >
                        <div className="background__overlay" style={{
                            backgroundColor: props.attributes.overlayColor,
                            opacity: props.attributes.overlayOpacity
                        }}></div>
                    { BackgroundOptionsVideoOutput( props ) }
                            <div className={ classnames(
                                'space',
                                props.attributes.wrapperWidth,
                            )} style={{height: props.attributes.space}}>
                                
                            </div>
                    </section>
            )
        }
	}
);
