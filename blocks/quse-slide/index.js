/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { TextControl, BaseControl, Dashicon, SelectControl, PanelBody, PanelRow, Button, ToggleControl } = wp.components;
const { InspectorControls, ColorPalette, MediaUpload, RichText } = wp.blockEditor;
const { AlignmentToolbar, BlockControls } = wp.editor;

/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

/**
 * Base Components
 */
import { BaseColors } from '../base';
import { HeadingType, ImageSizeOptions, ImagePositionOptions, CaptionStyleOptions, FlexOptionsHorizontal, FlexOptionsVertical } from '../base/components';

export default registerBlockType ("quse-blocks/quse-slide",
    {
        title: __("Slide"),
        description: __("A block that generates a Slide"),
        category: "quse-block-components",
        icon: "format-image",
        parent: ['quse-blocks/quse-carousel'],
        attributes: {
            title: {
				type: "string",
				source: "text",
				selector: ".slide__caption__title",
            },
            titleSize: {
                type: "string",
                default: 'heading-one'
            },
            titleColor: {
                type: 'string'
            },
			text: {
                type: 'array',
                source: 'children',
				selector: ".slide__caption__text"
			},
            textColor: {
                type: 'string'
            },
            slideimgURL: {
				type: "string",
				selector: ".slide__image"
			},
			slideimgID: {
				type: "number"
			},
			slideimgAlt: {
				type: "string",
				selector: ".slide__image"
            },
            imageSize: {
                type: 'string',
                default: 'cover',
            },
            imagePosition: {
                type: 'string',
                default: 'center',
            },
            alignment: {
                type: 'string',
                default: 'center',
            },
            slideLink: {
                type: 'string',
            },
            caption: {
                type: 'boolean',
                default: false,
            },
            captionStyle: {
                type: 'string',
            },
            captionWidth: {
                type: 'string',
                default: '500',
            },
            captionPositionHorizontal: {
                type: 'string',
                default: 'flex-start',
            },
            captionPositionVertical: {
                type: 'string',
                default: 'center',
            },
            blockAlignment: {
                default: 'full',
            },
        },
        
        edit: props => {

            const ColorControl = (props) => (
                <BaseControl
                    label={props.label}
                >
                    <ColorPalette
                        colors={BaseColors}
                        value={props.value}
                        onChange={props.onChange}
                    />
                </BaseControl>
            );
    
            const onSelectImage = img => {
				props.setAttributes({
					slideimgID: img.id,
					slideimgURL: img.url,
					slideimgAlt: img.alt
				});
			};
			const onRemoveImage = () => {
				props.setAttributes({
					slideimgID: null,
					slideimgURL: null,
					slideimgAlt: null
				});
            };

            const onChangeAlignment = value => props.setAttributes({alignment: value});
            const onChangeTitle = value => { props.setAttributes( { title: value } ) };
            const onChangeTitleSize = value => props.setAttributes({titleSize: value});
            const onChangeText = value => { props.setAttributes( { text: value } ) };
            const onChangeTitleColor = value => { props.setAttributes( { titleColor: value } ) };
            const onChangeTextColor = value => { props.setAttributes( { textColor: value } ) };
            const onChangeImageSize = value => props.setAttributes({imageSize: value});
            const onChangeImagePosition = value => props.setAttributes({imagePosition: value});
            const onChangeSlideLink = value => props.setAttributes({slideLink: value});

            const toggleCaption = value => props.setAttributes({caption: value});
            const toggleCaptionStyle = value => props.setAttributes({captionStyle: value});
            const onChangeCaptionWidth = value => props.setAttributes({captionWidth: value});
            const toggleCaptionPositionHorizontal = value => props.setAttributes({captionPositionHorizontal: value});
            const toggleCaptionPositionVertical = value => props.setAttributes({captionPositionVertical: value});

            return (
                <div className="slide">
                    <div className="badge-block-name">Slide</div>
                    <BlockControls>
                        <AlignmentToolbar
                            value={ props.attributes.alignment }
                            onChange={ onChangeAlignment }
                        />
                    </BlockControls>
                    { !props.attributes.slideimgID ? (
                        <InspectorControls key="inspector">
                            <PanelBody
                            title={ __( 'Slide Image' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                                <PanelRow>
                                    <div className="media-upload-wrapper">
                                        <MediaUpload
                                            onSelect={onSelectImage}
                                            type="image"
                                            className="slide_image"
                                            value={props.attributes.slideimgID}
                                            render={({ open }) => (
                                                <Button className="button button-large" onClick={ open }>
                                                    <Dashicon icon="format-image" /> { __( 'Upload Image' ) }
                                                </Button>
                                            )}
                                        />
                                    </div>
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Size: ' ) }
                                        value={props.attributes.imageSize}
                                        options={ImageSizeOptions}
                                        onChange={onChangeImageSize}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Position: ' ) }
                                        value={props.attributes.imagePosition}
                                        options={ImagePositionOptions}
                                        onChange={onChangeImagePosition}
                                    />
                                </PanelRow>
                            </PanelBody>
                        </InspectorControls>
                    ) : (
                        <InspectorControls key="inspector">
                            <PanelBody
                                title={ __( 'Slide Image' ) }
                                className="quse-background-options"
                                initialOpen={ false }
                            >
                                <PanelRow>
                                    <img
                                        src={ props.attributes.slideimgURL }
                                        alt={ props.attributes.slideimgAlt }
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <div className="media-button-wrapper">
                                        <p>
                                            <Button
                                                className="remove-image button button-large"
                                                onClick={ onRemoveImage }
                                            >
                                                <Dashicon icon="no-alt" /> { __( 'Remove Image' ) }
                                            </Button>
                                        </p>
                                    </div>
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Size: ' ) }
                                        value={props.attributes.imageSize}
                                        options={ImageSizeOptions}
                                        onChange={onChangeImageSize}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Position: ' ) }
                                        value={props.attributes.imagePosition}
                                        options={ImagePositionOptions}
                                        onChange={onChangeImagePosition}
                                    />
                                </PanelRow>
                            </PanelBody>
                        </InspectorControls>
                    )}
                    <InspectorControls key="inspector">
                        <PanelBody
                            title={ __( 'Slide Caption' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                            <ToggleControl
                                label={ __( 'Hide/Show Caption' ) }
                                checked={ !! props.attributes.caption }
                                onChange={toggleCaption}
                                help={ !! props.attributes.caption ? __( 'Showing on carousel.' ) : __( 'Hidden on carousel.' ) }
                            />
                            </PanelRow>
                            {props.attributes.caption &&
                            <div> 
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Title Size' ) }
                                        value={props.attributes.titleSize}
                                        options={HeadingType}
                                        onChange={onChangeTitleSize}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <ColorControl
                                        label={'Choose Title Color'}
                                        onChange={onChangeTitleColor}
                                        value={props.attributes.titleColor}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <ColorControl
                                        label={'Choose Text Color'}
                                        onChange={onChangeTextColor}
                                        value={props.attributes.textColor}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Caption Style' ) }
                                        value={props.attributes.captionStyle}
                                        options={CaptionStyleOptions}
                                        onChange={toggleCaptionStyle}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <TextControl
                                        label={'Set Caption Max Width'}
                                        value={props.attributes.captionWidth}
                                        onChange={onChangeCaptionWidth}
                                        help={'Default: 500(px)'}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Caption Position (Horizontal)' ) }
                                        value={props.attributes.captionPositionHorizontal}
                                        options={FlexOptionsHorizontal}
                                        onChange={toggleCaptionPositionHorizontal}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Caption Style (Vertical)' ) }
                                        value={props.attributes.captionPositionVertical}
                                        options={FlexOptionsVertical}
                                        onChange={toggleCaptionPositionVertical}
                                    />
                                </PanelRow>
                            </div>
                            }
                            
                        </PanelBody>
                        <PanelBody
                            title={ __( 'Slide Link' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <TextControl
                                    label={'Link Destination (URL)'}
                                    value={props.attributes.slideLink}
                                    onChange={onChangeSlideLink}
                                />
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>
                    <div className="slide__image carousel__item__image d-block w-100" style={{
                        backgroundImage: "url(" + props.attributes.slideimgURL + ")",
                        backgroundSize: props.attributes.slideimageSize,
                        backgroundPosition: props.attributes.imagePosition,
                    }}>
                        { ! props.attributes.slideimgID ? (
                                <MediaUpload
                                    onSelect={onSelectImage}
                                    type="image"
                                    value={props.attributes.slideimgID}
                                    render={ ( { open } ) => (
                                        <Button className="button button-large quse-media-button quse-media-button" onClick={ open }>
                                            <Dashicon icon="format-image" /> { __( 'Upload Image' ) }
                                        </Button>
                                    ) }
                                />
                            ) : (
                                <Button
                                    className="remove-image button button-large quse-media-button quse-media-button--overlay"
                                    onClick={ onRemoveImage }
                                >
                                    <Dashicon icon="no-alt" /> { __( 'Remove Image' ) }
                                </Button>
                            )}
                        <img class="slide__image--mobile" src={props.attributes.slideimgURL} alt={props.attributes.slideimgAlt} />
                    </div>
                    <div className="carousel-container" style={{
                        justifyContent: props.attributes.captionPositionHorizontal,
                        alignItems: props.attributes.captionPositionVertical,
                    }}>
                    { props.attributes.caption &&
                        <div className={ classnames(
                        "slide__caption",
                        "carousel-caption",
                        props.attributes.captionStyle,
                        ) } style={{
                            textAlign: props.attributes.alignment,
                            maxWidth: props.attributes.captionWidth + 'px',
                        }}>
                            <div className={ classnames(
                                    'slide__caption__title',
                                    props.attributes.titleSize,
                                )}
                                style={{
                                    color: props.attributes.titleColor
                            }}>
                                <RichText
                                    value={ props.attributes.title } // Any existing content, either from the database or an attribute default
                                    allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                                    onChange={onChangeTitle} // Store updated content as a block attribute
                                    placeholder={ __( 'Enter Title...' ) } // Display this text before any content has been added by the user
                                />
                            </div>
                            <div className="slide__caption__text carousel__item__text d-none d-md-block" style={{
                                color: props.attributes.textColor
                            }}>
                                <RichText
                                    value={ props.attributes.text } // Any existing content, either from the database or an attribute default
                                    allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                                    onChange={onChangeText} // Store updated content as a block attribute
                                    placeholder={ __( 'Enter your text here...' ) } // Display this text before any content has been added by the user
                                />
                            </div>
                        </div>
                    }
                    </div>
                </div>
			);
		},

		save: props => {
			return (
				<a className="slide carousel-item" href={props.attributes.slideLink}>
                    {props.attributes.slideimgURL &&
                        <div className="slide__image carousel__item__image d-block w-100">
                            <img class="slide__image--featured" src={props.attributes.slideimgURL} alt={props.attributes.slideimgAlt} />
                        </div>
                    }
                    <div class="slide__inner">
                        <div className="carousel-container" style={{
                            justifyContent: props.attributes.captionPositionHorizontal,
                            alignItems: props.attributes.captionPositionVertical,
                        }}>
                            { props.attributes.caption &&

                                <div className={ classnames(
                                    "slide__caption",
                                    "carousel-caption",
                                    props.attributes.captionStyle,
                                    ) } style={{
                                        textAlign: props.attributes.alignment,
                                        maxWidth: props.attributes.captionWidth + 'px',
                                }}>

                                    <div className={ classnames(
                                        'slide__caption__title',
                                        props.attributes.titleSize,
                                    )} 
                                    style={{
                                        color: props.attributes.titleColor
                                    }}>
                                        {props.attributes.title}
                                    </div>
                                    
                                    <div className="slide__caption__text carousel__item__text d-md-block" style={{
                                        color: props.attributes.textColor
                                    }}>
                                        {props.attributes.text}
                                    </div>
                                </div>

                            }

                        </div>
                    </div>
				</a>
			);
		}
	}
);
