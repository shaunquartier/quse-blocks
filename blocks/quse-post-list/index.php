<?php

namespace Quse_BLOCKS\Blocks\Quse_Post_List;

add_action( 'plugins_loaded', __NAMESPACE__ . '\register_post_list_block' );
/**
 * Register the post list block.
 *
 * @since 2.1.0
 *
 * @return void
 */
function register_post_list_block() {

	// Only load if Gutenberg is available.
	if ( ! function_exists( 'register_block_type' ) ) {
		return;
	}

	// Hook server side rendering into render callback
	register_block_type( 'quse-blocks/quse-post-list', array(
        'render_callback' => __NAMESPACE__ . '\render_post_list_block',
    ) );

}

/**
 * Server rendering for /blocks/quse-post-list
 */
function render_post_list_block( $att ) {

    // If the experience is set AND doesn't equal the session then RETURN nothing
    if ( (!empty($att['experience'])) && ($att['experience'] !== $_SESSION['experience']) ) {

        return "";

    } else {
        
        $recent_posts = wp_get_recent_posts( [
            'numberposts'   => ( !empty($att['postsPerPage']) ? $att['postsPerPage'] : '-1'),
            'order'         => ( !empty($att['postsOrder']) ? $att['postsOrder'] : 'desc'),
            'orderby'       => ( !empty($att['postsOrderBy']) ? $att['postsOrderBy'] : 'date'),
            'post_status'   => 'publish',
        ] );
    
        if ( empty( $recent_posts ) ) {
            return '<p>No posts</p>';
        }
        
        $post_item = "";
    
        foreach ( $recent_posts as $post ) {
            $post_id  = $post['ID'];
            $post_categories = array();
    
            $get_categories = get_the_category( $post_id );
            foreach ($get_categories as $categories) {
                array_push($post_categories, $categories->name); 
            }
            $post_categories = implode("</a> <a href='#' class='category-item'>", $post_categories);
    
                if (!empty($att['postStyle']) && $att['postStyle'] == 'simple') {
    
                    $post_item .= '<div class="post display--simple">';
                        $post_item .= '<a class="post__link" href="' . esc_url( get_permalink( $post_id ) ) . '">';
                            $post_item .= '<div class="post__title display-title-simple">' . esc_html( get_the_title( $post_id ) ) . '</div>';
                        $post_item .= '</a>';    
                    $post_item .= '</div>';
    
                } else {
    
                    $post_item .= '<div class="post display">';
                        $post_item .= '<div class="post__image display-image-top">' . get_the_post_thumbnail( $post_id, 'full' ) . '</div>';
                        $post_item .= '<div class="post__body display-body">';
                            $post_item .= '<div class="post__body__content display-body__content">';
                                $post_item .= '<div class="post__title display-title heading-two">' . esc_html( get_the_title( $post_id ) ) . '</div>';
                                if ( !empty($att['hide_show_date']) && $att['hide_show_date'] == true ) {
                                    $post_item .= '<div class="post__date">' . get_the_date('M d Y', $post_id) . '</div>';
                                }
                                $post_item .= '<div class="post__excerpt display-text">' . esc_html( get_the_excerpt( $post_id ) ) . '</div>';
                                if ( !empty($att['hide_show_date']) && $att['hide_show_categories'] == true ) {
                                    $post_item .= '<div class="post__categories">Categories: <a href="#" class="category-item">' . $post_categories . '</span></div>';
                                }
                                $post_item .= '<a class="post__link quse__button quse__button--clear btn" href="' . esc_url( get_permalink( $post_id ) ) . '">Read More</a>';
                            $post_item .= '</div>';
                        $post_item .= '</div>';
                    $post_item .= '</div>';
    
                }
    
        }
    
        return "<div class='post-list container-wrapper'>{$post_item}</div>";

    }

	
}
