/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement, Fragment} = wp.element;
const { InspectorControls, InnerBlocks } = wp.blockEditor;
const { BlockControls, BlockAlignmentToolbar } = wp.editor;
const { BaseControl, PanelBody, PanelRow, SelectControl, Spinner, TextControl, ToggleControl } = wp.components;
const { withSelect } = wp.data;

/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

/**
 * Base Components
 */
import BackgroundOptions, { BackgroundOptionsAttributes, BackgroundOptionsClasses, BackgroundOptionsInlineStyles, BackgroundOptionsVideoOutput } from '../base';

export const OrderByOptions = [
    {label: 'Date', value: 'date'},
    {label: 'Author', value: 'author'},
];

export const OrderOptions = [
    {label: 'DESC', value: 'desc'},
    {label: 'ASC', value: 'asc'},
];

export const ListStyle = [
    {label: 'Full (Title/Desc/Link/Image)', value: 'full'},
    {label: 'Simple (Title/Link)', value: 'simple'},
];

export default registerBlockType ("quse-blocks/quse-post-list",
    {
        title: __("Post List"),
        description: __("A block that will show a list of WordPress Posts."),
        category: "quse-block-layouts",
        icon: "list-view",
        attributes: {
            postsPerPage: {
                type: 'string',
                default: '10',
            },
            postsOrderBy: {
                type: 'string',
                default: 'date',
            },
            postsOrder: {
                type: 'string',
                default: 'desc',
            },
            postsCategory: {
                type: 'string',
            },
            postsTag: {
                type: 'string',
            },
            category_selected: {
                type: 'string',
            },
            post_type_selected: {
                type: 'string',
            },
            hide_show_date: {
                type: 'boolean',
                default: false,
            },
            hide_show_categories: {
                type: 'boolean',
                default: false,
            },
            postStyle: {
                type: 'string',
            },
            ...BackgroundOptionsAttributes,
        },

        edit: withSelect( (select, props) => {

            const currentPostId = select('core/editor').getCurrentPostId();
            var pt;
            const query = {
                per_page: props.attributes.postsPerPage, // Default 10, set to -1 for the max of 100,
                orderby: props.attributes.postsOrderBy,
                order: props.attributes.postsOrder,
                exclude: currentPostId, // Exclude the current post if using it there
            }

            if (props.attributes.category_selected != -1) {
                query["categories"] = props.attributes.category_selected;
            } else {
                delete query[4]; // Removes the last item in the array which would be the category
            }

            if (props.attributes.post_type_selected !== -1) {
                pt = props.attributes.post_type_selected;
            }

            //console.log(query);

            return {
                posts: select( 'core' ).getEntityRecords( 'postType', pt ? pt : 'post', query ),
                categories: select('core').getEntityRecords('taxonomy', 'category'),
                post_types: select('core').getPostTypes()
            };

        } )( ( { posts, categories, post_types, className, isSelected, setAttributes, attributes } ) => {

            var postsPerPage = attributes.postsPerPage;
            var postsOrder = attributes.postsOrder;
            var postsOrderBy = attributes.postsOrderBy;
            var category_selected = attributes.category_selected;
            var post_type_selected = attributes.post_type_selected;
            var postStyle = attributes.postStyle;

            const onChangePostsPerPage = value => setAttributes({postsPerPage: value});
            const onChangePostsOrder = value => setAttributes({postsOrder: value});
            const onChangePostsOrderBy = value => setAttributes({postsOrderBy: value});
            const onChangeCategory = value => setAttributes({category_selected: value});
            const onChangePostType = value => setAttributes({post_type_selected: value});
            const onChangePostStyle = value => setAttributes({postStyle: value});
            const onChangeExperience = value => setAttributes({experience: value});

            const togglePostDate = value => setAttributes({hide_show_date: value});
            const togglePostCategories = value => setAttributes({hide_show_categories: value});


            if ( ! posts ) {
                return (
                    <div>
                        <p className={className} >
                            <Spinner />
                            { __( 'Loading Posts', 'quse-blocks' ) }
                        </p>
                    </div>
                );
            }

            //console.log(post_types);
            var set_post_types = [];

            if (post_types) {
                    
                    post_types.forEach((post_type) => { // simple foreach loop

                        post_types.indexOf(post_type.name) === -1 ? set_post_types.push({value:post_type.slug, label:post_type.name}) : console.log("This item already exists");
                        
                    });

            } else {
                set_post_types.push( { value: 'post', label: 'Posts' } )
            }

            //console.log(categories);

            var set_categories = [];

            if (categories) {

                    set_categories.push( { value: -1, label: 'Show All' } );
                    
                    categories.forEach((category) => { // simple foreach loop

                        categories.indexOf(category.name) === -1 ? set_categories.push({value:category.id, label:category.name}) : console.log("This item already exists");
                        
                    });

            } else {
                set_categories.push( { value: 0, label: 'Loading...' } )
            }

            return (
                <div>
                    <div className="badge-block-name">Post List</div>
                    <InspectorControls key="inspector">
                        <PanelBody
                            title={ __('Block Settings')}
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                            <TextControl
                                label={"Set Block Experience"}
                                value={attributes.experience}
                                onChange={onChangeExperience}
                            />
                            </PanelRow>
                        </PanelBody>
                        <PanelBody
                            title={ __( 'Post List Options' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <SelectControl 
                                    label={'Choose a List Style'}
                                    options={ListStyle}
                                    value={postStyle}
                                    onChange={onChangePostStyle}
                                />
                            </PanelRow>
                            <PanelRow>
                                <SelectControl 
                                    label={'Select a Post Type'}
                                    options={set_post_types}
                                    value={post_type_selected}
                                    onChange={onChangePostType}
                                />
                            </PanelRow>
                            <PanelRow>
                                <SelectControl 
                                    label={'Select a Category'}
                                    options={set_categories}
                                    value={category_selected}
                                    onChange={onChangeCategory}
                                />
                            </PanelRow>
                            <PanelRow>
                                <TextControl
                                    label={"Posts Per Page "}
                                    className={"small-box"}
                                    value={postsPerPage}
                                    help={"10 posts are shown by default; Max is 100"}
                                    onChange={onChangePostsPerPage}
                                />
                            </PanelRow>
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Order By' ) }
                                    value={postsOrderBy}
                                    options={OrderByOptions}
                                    onChange={onChangePostsOrderBy}
                                />
                            </PanelRow>
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Order' ) }
                                    value={postsOrder}
                                    options={OrderOptions}
                                    onChange={onChangePostsOrder}
                                />
                            </PanelRow>
                        </PanelBody>
                        { attributes.postStyle == 'full' && 
                            <PanelBody
                                title={ __( 'Show/Hide Options' ) }
                                className="quse-background-options"
                                initialOpen={ false }
                            >
                                <ToggleControl
                                    label={ __( 'Hide/Show Date' ) }
                                    checked={ !! attributes.hide_show_date }
                                    onChange={togglePostDate}
                                />  
                                <ToggleControl
                                    label={ __( 'Hide/Show Categories' ) }
                                    checked={ !! attributes.hide_show_categories }
                                    onChange={togglePostCategories}
                                />  
                            </PanelBody>
                        }
                        
                    </InspectorControls>
                    <div className='post-list'>
                        { posts.map( post => {

                            //console.log(post);

                            if (categories) {
                                var postCategory = [];
                                categories.forEach((post_category) => { // simple foreach loop
                                    if (post.categories.indexOf(post_category.id) !== -1) {
                                        postCategory.push(post_category.name);
                                    } 
                                });
                                var cat_string = postCategory.join(', ');
                            }
                            
                            if ( post.featured_media !== 0) {
                                var featuredmedia = post._links["wp:featuredmedia"]["0"].href;
                                fetch(featuredmedia)
                                .then(response => response.json())
                                .then(function(data) {
                                    // Set the featured image of the post id that it matches
                                    var featImg = document.getElementById("featured-image-" + post.id);
                                    featImg.src = data.source_url;
                                    return;
                                })
                                .catch(function(error) {
                                    console.log('Error: ' + error);
                                });
                                
                            }

                            return (
                                <div>
                                    { attributes.postStyle == 'full' ? (
                                        <div className="post display">
                                            <div className="badge-block-name"><a target="_blank" href={"/wp-admin/post.php?post=" + post.id + "&action=edit"}>Edit Post</a></div>
                                            <div className="post__image display-image-top"><img id={"featured-image-" + post.id} /></div>
                                            <div className="post__body display-body">
                                                <div className="post__body__content display-body__content">
                                                    <div className="post__title display-title heading-two">
                                                        {post.title.raw}
                                                    </div>
                                                    {attributes.hide_show_date &&
                                                        <div className="post__date">
                                                            {moment(post.date).format('MMMM D Y')}
                                                        </div>
                                                    }
                                                    <div className="post__excerpt display-text">
                                                        {post.excerpt.raw}
                                                    </div>
                                                    {post.categories != 1 && attributes.hide_show_categories && 
                                                        <div className="post__categories">
                                                            Categories: {cat_string}
                                                        </div>
                                                    }
                                                    <a className="post__link quse__button quse__button--clear btn" href="#">
                                                        Read More
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    ) : (
                                        <div className="post display--simple">
                                            <div className="badge-block-name"><a target="_blank" href={"/wp-admin/post.php?post=" + post.id + "&action=edit"}>Edit Post</a></div>
                                            <a className="post__link" href="#">
                                                <div className="post__title display-title-simple">
                                                    {post.title.raw}
                                                </div>
                                            </a>
                                        </div>
                                    )}
                                </div>
                            );
                        }) }
                    </div>
                </div>
            );
        } ) // end withAPIData
        ,

		save(props, postsPerPage, postsOrder, postsOrderBy) {
            // Rendering in PHP
            return null;
        },
    } );
