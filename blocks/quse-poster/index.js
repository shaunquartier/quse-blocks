/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { TextControl, BaseControl, PanelBody, PanelRow, SelectControl, Dashicon, Button, RangeControl } = wp.components;
const { InspectorControls, ColorPalette, InnerBlocks, MediaUpload, RichText } = wp.blockEditor;
const { AlignmentToolbar, BlockControls, BlockAlignmentToolbar } = wp.editor;

/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

/**
 * Base Components
 */
import BackgroundOptions, { BackgroundOptionsAttributes, BaseColors, BackgroundOptionsClasses, BackgroundOptionsInlineStyles, BackgroundOptionsVideoOutput, ExperienceClasses } from '../base';
import { HeadingType, ImageSizeOptions, ImagePositionOptions } from '../base/components';

export default registerBlockType ("quse-blocks/quse-poster",
    {
        title: __("Poster Display"),
        description: __("A block that displays a poster layout for your page."),
        category: "quse-block-layouts",
        icon: "tablet",
        attributes: {
            title: {
				type: "string",
				source: "text",
				selector: ".poster__title",
				default: "Poster Title"
            },
            titleSize: {
                type: "string",
                default: 'heading-one'
            },
            titleColor: {
                type: 'string'
            },
			content: {
                type: 'array',
                source: 'children',
                selector: ".poster__text",
			},
            contentColor: {
                type: 'string'
            },
            posterimgURL: {
				type: "string",
				selector: ".poster__image"
			},
			posterimgID: {
				type: "number"
			},
			posterimgAlt: {
				type: "string",
				selector: ".poster__image"
            },
            posterSize: {
                type: 'string',
                default: 'cover',
            },
            posterPosition: {
                type: 'string',
                default: 'center',
            },
            posterImageHeight: {
                type: 'string',
                default: null,
            },
            posterOverlayColor: {
                type: 'string',
            },
            posterOverlayOpacity: {
                type: 'integer',
            },
            alignment: {
                type: 'string',
            },
            position: {
                type: 'string',
            },
            ...BackgroundOptionsAttributes,
        },
        
        edit: props => {

            const ALLOWED_BLOCKS = [ 'quse-blocks/quse-link' ];

            const onSelectImage = img => {
				props.setAttributes({
					posterimgID: img.id,
					posterimgURL: img.url,
					posterimgAlt: img.alt
				});
			};
			const onRemoveImage = () => {
				props.setAttributes({
					posterimgID: null,
					posterimgURL: null,
					posterimgAlt: null
				});
            };

            const onRemoveImageHeight = () => props.setAttributes({ posterImageHeight: null });
            const onSetImageHeight = () => props.setAttributes({ posterImageHeight: '200' });

            const onChangeTitle = value => { props.setAttributes( { title: value } ) };
            const onChangeTitleSize = value => props.setAttributes({titleSize: value});
            const onChangeContent = value => { props.setAttributes( { content: value } ) };
            const onChangeTitleColor = value => { props.setAttributes( { titleColor: value } ) };
            const onChangeContentColor = value => { props.setAttributes( { contentColor: value } ) };
            const onChangeAlignment = value => props.setAttributes({alignment: value});
            const onChangePosition = value => props.setAttributes({position: value});
            const onChangeImageSize = value => props.setAttributes({posterSize: value});
            const onChangeImagePosition = value => props.setAttributes({posterPosition: value});
            const onChangeBlockSize = value => props.setAttributes({blockAlignment: value});

            const setposterOverlayColor = value => props.setAttributes( { posterOverlayColor: value } );
            const onChangeposterOpacity = value => props.setAttributes( { posterOverlayOpacity: value } );
            const onChangePosterImageHeight = value => props.setAttributes({posterImageHeight: value});

            const ColorControl = (props) => (
                <BaseControl
                    label={props.label}
                >
                    <ColorPalette
                        colors={BaseColors}
                        value={props.value}
                        onChange={props.onChange}
                    />
                </BaseControl>
            );

            const positionOptions = [
                { label: 'Left Side', value: 'flex-start' },
                { label: 'Center', value: 'center' },
                { label: 'Right Side', value: 'flex-end' },
            ];
        
            return (
                <div>
                    <div className="badge-block-name">Poster Layout</div>
                    <BlockControls>
                        <BlockAlignmentToolbar
                            value={ props.attributes.blockAlignment }
                            onChange={ onChangeBlockSize }
                        />
                        <AlignmentToolbar
                            value={ props.attributes.alignment }
                            onChange={ onChangeAlignment }
                        />
                    </BlockControls>
                    <InspectorControls key="inspector">
                        { BackgroundOptions( props ) }
                        { !props.attributes.posterimgID ? (
                                <PanelBody
                                title={ __( 'Poster Image' ) }
                                className="quse-background-options"
                                initialOpen={ false }
                            >
                                <PanelRow>
                                    <div className="media-upload-wrapper">
                                        <MediaUpload
                                            onSelect={onSelectImage}
                                            type="image"
                                            className="poster_image"
                                            value={props.attributes.posterimgID}
                                            render={({ open }) => (
                                                <Button className="button button-large" onClick={ open }>
                                                    <Dashicon icon="format-image" /> { __( 'Upload Image' ) }
                                                </Button>
                                            )}
                                        />
                                    </div>
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Size: ' ) }
                                        value={props.attributes.posterSize}
                                        options={ImageSizeOptions}
                                        onChange={onChangeImageSize}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Position: ' ) }
                                        value={props.attributes.posterPosition}
                                        options={ImagePositionOptions}
                                        onChange={onChangeImagePosition}
                                    />
                                </PanelRow>
                            </PanelBody>
                        ) : (
                            <PanelBody
                                title={ __( 'Poster Image' ) }
                                className="quse-background-options"
                                initialOpen={ false }
                            >
                                <PanelRow>
                                        <img
                                            src={ props.attributes.posterimgURL }
                                            alt={ props.attributes.posterimgAlt }
                                        />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Size: ' ) }
                                        value={props.attributes.posterSize}
                                        options={ImageSizeOptions}
                                        onChange={onChangeImageSize}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Position: ' ) }
                                        value={props.attributes.posterPosition}
                                        options={ImagePositionOptions}
                                        onChange={onChangeImagePosition}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <TextControl
                                            label={"Image Height: "}
                                            className={"small-box"}
                                            help={"Set Update the height above in (px) or set to 100% below"}
                                            value={props.attributes.posterImageHeight}
                                            onChange={onChangePosterImageHeight}
                                        />
                                    </PanelRow>
                                    <PanelRow>
                                    <ColorControl
                                        label={'Overlay Color'}
                                        onChange={setposterOverlayColor}
                                        value={props.attributes.posterOverlayColor}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <RangeControl
                                        label="Overlay Opacity"
                                        value={ props.attributes.posterOverlayOpacity }
                                        help={"Must be between 0 - 1"}
                                        onChange={ onChangeposterOpacity }
                                        min={ 0 }
                                        max={ 1 }
                                        step={ 0.05 }
                                    />
                                </PanelRow>
                            </PanelBody>

                        )}
                        <PanelBody
                            title={ __( 'Poster Content' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Content Position' ) }
                                    help={"Adjust where you want your content."}
                                    value={props.attributes.position}
                                    options={positionOptions}
                                    onChange={onChangePosition}
                                />
                            </PanelRow>
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Title Size' ) }
                                    value={props.attributes.titleSize}
                                    options={HeadingType}
                                    onChange={onChangeTitleSize}
                                />
                            </PanelRow>
                            <PanelRow>
                                <ColorControl
                                    label={'Title Color'}
                                    onChange={onChangeTitleColor}
                                    value={props.attributes.titleColor}
                                />
                            </PanelRow>
                            <PanelRow>
                                <ColorControl
                                    label={'Content Color'}
                                    onChange={onChangeContentColor}
                                    value={props.attributes.contentColor}
                                />
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>
                    <section
                        className={ classnames(
                            'quse-block',
                            'poster',
                            ...BackgroundOptionsClasses( props ),
                            ExperienceClasses(props),
                        ) } style={ {
                            ...BackgroundOptionsInlineStyles( props ),
                        } }>
                            
                        <div className="background__overlay" style={{
                            backgroundColor: props.attributes.overlayColor,
                            opacity: props.attributes.overlayOpacity
                        }}></div>

                            { BackgroundOptionsVideoOutput( props ) }
                            <div className={ classnames(
                                'poster__inner',
                                'container--' + props.attributes.blockAlignment,
                            )} style={{
                                textAlign: props.attributes.alignment,
                                justifyContent: props.attributes.position,
                                }}>
                                    <div className={ classnames(
                                        'poster__inner__image',
                                        'd-block',
                                        'w-100',
                                    )} style={{
                                        height: null !== props.attributes.posterImageHeight ? props.attributes.posterImageHeight + 'px' : null,
                                        backgroundImage: "url(" + props.attributes.posterimgURL + ")",
                                        backgroundSize: props.attributes.posterSize,
                                        backgroundPosition: props.attributes.posterPosition,
                                    }}>
                                        <div className="background__overlay" style={{
                                            backgroundColor: props.attributes.posterOverlayColor,
                                            opacity: props.attributes.posterOverlayOpacity
                                        }}></div>
                                    { ! props.attributes.posterimgID ? (
                                        <MediaUpload
                                            onSelect={onSelectImage}
                                            type="image"
                                            className="poster_image"
                                            value={props.attributes.posterimgID}
                                            render={({ open }) => (
                                                <Button className="button button-large quse-media-button quse-media-button" onClick={ open }>
                                                    <Dashicon icon="format-image" /> { __( 'Upload Image' ) }
                                                </Button>
                                            )}
                                        />
                                    ) : (
                                        <Button
                                            className="remove-image button button-large quse-media-button quse-media-button--overlay"
                                            onClick={ onRemoveImage }
                                        >
                                            <Dashicon icon="no-alt" /> { __( 'Remove Image' ) }
                                        </Button>
                                    )}
                                    </div>
                                
                                <div className="poster__inner__content">
                                    <div className={ classnames(
                                        'poster__title',
                                        props.attributes.titleSize,
                                    )}
                                    style={{color: props.attributes.titleColor}}>
                                        <RichText
                                            value={ props.attributes.title } // Any existing content, either from the database or an attribute default
                                            allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                                            onChange={onChangeTitle} // Store updated content as a block attribute
                                        />
                                    </div>
                                    <div className="poster__text" style={{
                                        color: props.attributes.contentColor
                                    }}>
                                        <RichText
                                            value={ props.attributes.content } // Any existing content, either from the database or an attribute default
                                            allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                                            onChange={onChangeContent} // Store updated content as a block attribute
                                            placeholder={ __( 'Enter your content here...' ) } // Display this text before any content has been added by the user
                                        />
                                    </div>
                                    <InnerBlocks allowedBlocks={ ALLOWED_BLOCKS } />
                                </div>
                            </div>
                    </section>

                </div>
                )
		},

		save: props => {
            return (
                <section className={ classnames(
                    'quse-block',
                    'poster',
                    ...BackgroundOptionsClasses( props ),
                    ExperienceClasses(props),
                ) }
                style={ {
                    ...BackgroundOptionsInlineStyles( props ),
                } }>
                    
                    <div className="background__overlay" style={{
                        backgroundColor: props.attributes.overlayColor,
                        opacity: props.attributes.overlayOpacity
                    }}></div>

                    { BackgroundOptionsVideoOutput( props ) }

                    <div className={ classnames(
                        'poster__inner',
                        'container--' + props.attributes.blockAlignment,
                    )} style={{
                        textAlign: props.attributes.alignment,
                        justifyContent: props.attributes.position,
                    }}>

                        {props.attributes.posterimgURL &&
                            <div className={ classnames(
                                'poster__inner__image',
                                'd-block',
                                'w-100',
                            )}
                            style={{
                                height: null !== props.attributes.posterImageHeight ? props.attributes.posterImageHeight + 'px' : null,
                                backgroundImage: "url(" + props.attributes.posterimgURL + ")", 
                                backgroundSize: props.attributes.posterSize,
                                backgroundPosition: props.attributes.posterPosition,
                            }}>
                                <div className="background__overlay" style={{
                                    backgroundColor: props.attributes.posterOverlayColor,
                                    opacity: props.attributes.posterOverlayOpacity
                                }}></div>
                            </div>
                        }

                        <div className="poster__inner__content">

                            <div className={ classnames(
                                'poster__title',
                                props.attributes.titleSize,
                            )} style={{color: props.attributes.titleColor}}>
                                {props.attributes.title}
                            </div>

                            <div className="poster__text" style={{color: props.attributes.contentColor}}>
                                {props.attributes.content}
                            </div>
                            
                            <InnerBlocks.Content />
                        </div>
                    </div>
                </section>
            )
        }
	}
);
