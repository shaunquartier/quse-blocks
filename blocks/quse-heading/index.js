/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { BaseControl, PanelBody, PanelRow, SelectControl } = wp.components;
const {InspectorControls, ColorPalette, RichText } = wp.blockEditor;
const { AlignmentToolbar, BlockControls, BlockAlignmentToolbar } = wp.editor;

/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

/**
 * Base Components
 */
import BackgroundOptions, { BackgroundOptionsAttributes, BaseColors, BackgroundOptionsClasses, BackgroundOptionsInlineStyles, BackgroundOptionsVideoOutput, ExperienceClasses } from '../base';
import { HeadingType } from '../base/components';

/**
 * Layout Components
 */


export default registerBlockType ("quse-blocks/quse-heading",
    {
        title: __("Heading Display"),
        description: __("A block that displays a heading for your page."),
        category: "quse-block-layouts",
        icon: "schedule",
        attributes: {
            title: {
				type: "string",
				source: "text",
				selector: ".heading__inner__title",
				default: "Heading Title"
            },
            titleColor: {
                type: 'string'
            },
            titleSize: {
                type: "string",
                default: 'heading-one'
            },
			subtitle: {
                type: "string",
				source: "text",
                selector: ".heading__inner__subtitle",
                default: "Heading Subtitle"
			},
            subtitleColor: {
                type: 'string'
            },
            subtitleSize: {
                type: "string",
                default: "heading-five"
            },
            alignment: {
                type: 'string',
            },
            postDate: {
                type: 'string',
            },
            postCategories: {
                type: 'string',
            },
            ...BackgroundOptionsAttributes,
        },
        
        edit: props => {

            const onChangeTitle = value => { props.setAttributes( { title: value } ) };
            const onChangeSubtitle = value => { props.setAttributes( { subtitle: value } ) };
            const onChangeTitleColor = value => { props.setAttributes( { titleColor: value } ) };
            const onChangeSubtitleColor = value => { props.setAttributes( { subtitleColor: value } ) };
            const onChangeAlignment = value => props.setAttributes({alignment: value});
            const onChangeBlockSize = value => props.setAttributes({blockAlignment: value});
            const onChangeTitleSize = value => props.setAttributes({titleSize: value});
            const onChangeSubTitleSize = value => props.setAttributes({subtitleSize: value});

            const ColorControl = (props) => (
                <BaseControl
                    label={props.label}
                >
                    <ColorPalette
                        colors={BaseColors}
                        value={props.value}
                        onChange={props.onChange}
                    />
                </BaseControl>
            );
        
            return (
                <div>
                    <div className="badge-block-name">Heading Layout</div>
                    <BlockControls>
                        <BlockAlignmentToolbar
                            value={ props.attributes.blockAlignment }
                            onChange={ onChangeBlockSize }
                        />
                        <AlignmentToolbar
                            value={ props.attributes.alignment }
                            onChange={ onChangeAlignment }
                        />
                    </BlockControls>
                    <InspectorControls key="inspector">
                        { BackgroundOptions( props ) }
                        <PanelBody
                            title={ __( 'Heading Title' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Size' ) }
                                    value={props.attributes.titleSize}
                                    options={HeadingType}
                                    onChange={onChangeTitleSize}
                                />
                            </PanelRow>
                            <PanelRow>
                                <ColorControl
                                    label={'Title Color'}
                                    onChange={onChangeTitleColor}
                                    value={props.attributes.titleColor}
                                />
                            </PanelRow>
                        </PanelBody>
                        <PanelBody
                            title={ __( 'Heading Subtitle' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Size' ) }
                                    value={props.attributes.subtitleSize}
                                    options={HeadingType}
                                    onChange={onChangeSubTitleSize}
                                />
                            </PanelRow>
                            <PanelRow>
                                <ColorControl
                                    label={'Color'}
                                    onChange={onChangeSubtitleColor}
                                    value={props.attributes.subtitleColor}
                                />
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>
                    <section className={ classnames(
                        'quse-block',
                        'heading',
                        ...BackgroundOptionsClasses( props ),
                        ExperienceClasses(props),
                    ) } style={ { ...BackgroundOptionsInlineStyles( props ) } }>

                        <div className="background__overlay" style={{
                            backgroundColor: props.attributes.overlayColor,
                            opacity: props.attributes.overlayOpacity
                        }}></div>

                        { BackgroundOptionsVideoOutput( props ) }

                        <div className={ classnames(
                            'heading__inner',
                            'container--' + props.attributes.blockAlignment,
                        )} style={{textAlign: props.attributes.alignment}}>
                            <div className={ classnames(
                                'heading__inner__title',
                                props.attributes.titleSize,
                            )}
                            style={{color: props.attributes.titleColor}}>
                                <RichText
                                    value={ props.attributes.title } // Any existing content, either from the database or an attribute default
                                    allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                                    onChange={onChangeTitle} // Store updated content as a block attribute
                                    placeholder={"Enter Heading Title"}
                                />
                            </div>
                            <div className={ classnames(
                                'heading__inner__subtitle',
                                props.attributes.subtitleSize,
                            )} style={{ color: props.attributes.subtitleColor}}>
                                <RichText
                                    value={ props.attributes.subtitle } // Any existing content, either from the database or an attribute default
                                    allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                                    onChange={onChangeSubtitle} // Store updated content as a block attribute
                                    placeholder={"Enter Heading Subtitle"}
                                />
                            </div>
                        </div>
                    </section>

                </div>
                )
		},

        save: props => {
            return (
                    <section className={ classnames(
                        'quse-block',
                        'heading',
                        ...BackgroundOptionsClasses( props ),
                        ExperienceClasses(props),
                    ) } style={ {...BackgroundOptionsInlineStyles( props )} } >
                        
                    <div className="background__overlay" style={{
                        backgroundColor: props.attributes.overlayColor,
                        opacity: props.attributes.overlayOpacity
                    }}></div>
                      
                    { BackgroundOptionsVideoOutput( props ) }

                    <div className={ classnames(
                        'heading__inner',
                        'container--' + props.attributes.blockAlignment,
                    )} style={{textAlign: props.attributes.alignment}}>

                        {props.attributes.postCategories &&
                            <div className="heading__inner__categories">
                                {props.attributes.postCategories}
                            </div>
                        }

                        <div className={ classnames(
                            'heading__inner__title',
                            props.attributes.titleSize,
                        )} style={{color: props.attributes.titleColor}}>
                            {props.attributes.title}
                        </div>

                        <div className={ classnames(
                            'heading__inner__subtitle',
                            props.attributes.subtitleSize,
                        )} style={{color: props.attributes.subtitleColor}}>
                            {props.attributes.subtitle}
                        </div>

                        {props.attributes.postDate &&
                            <div className="heading__inner__date">
                                <em>{moment(props.attributes.postDate).format('MMMM D Y')}</em>
                            </div>
                        }

                    </div>
                </section>
            )
        }
	}
);
