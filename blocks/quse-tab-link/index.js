/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { TextControl, PanelBody, PanelRow } = wp.components;
const { InspectorControls } = wp.blockEditor;
const { AlignmentToolbar, BlockControls, BlockAlignmentToolbar } = wp.editor;

export default registerBlockType ("quse-blocks/quse-tab-link",
    {
        title: __("Tab Link"),
        description: __("A block that generates a tab link. Choose how you want the block to display in the settings below."),
        category: "quse-block-components",
        icon: "admin-links",
        parent: ['quse-blocks/quse-nav'],
        attributes: {
            linkText: {
                type: 'string',
                source: 'text',
                selector: 'a',
                default: 'Tab Title'
            },
            link: {
                type: 'string',
                source: 'attribute',
                selector: 'a.nav-link',
                attribute: 'href'
            },
            linkName: {
                type: 'string',
            },
            tabId: {
                type: 'string',
            },
        },
        
        edit: props => {

            const onChangeLink = value => props.setAttributes({link: value});
            const onChangeLinkText = value => props.setAttributes({linkText: value});

            var myStr = props.attributes.linkText

            if (props.attributes.linkText) {
                myStr = myStr.replace(/\s/g, "").toLowerCase();
            }

            props.setAttributes( { link: '#' + myStr } );
            props.setAttributes( { linkName: myStr + '-tab' } );

            return (
                <div>
                    <div className="badge-block-name">Tab Link: {myStr}</div>
                    <InspectorControls key="inspector">
                        <PanelBody
                            title={ __( 'Tab Link Settings' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <TextControl
                                    label={"Link Name"}
                                    value={props.attributes.linkText}
                                    onChange={onChangeLinkText}
                                />
                            </PanelRow>
                            <PanelRow>
                                <div>
                                    <p>Use the tab name below to match this tab with a content block. </p>
                                    <br />
                                    <p><strong>Tab Name:</strong> {myStr}</p>
                                    <br />
                                    <p>Click on the content layout you want to link this tab to, and then copy the name above and paste into the "Tab ID" field in the Content Settings.</p>
                                </div>
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>
                    <a
                        id={props.attributes.tabId}
                        className='nav-link quse__tab'
                        rel="noopener noreferrer"
                        aria-pressed="true"
                        title={props.attributes.link}
                        aria-controls={props.attributes.linkName}
                        role='tab'
                    >
                        {props.attributes.linkText}
                    </a>
                </div>
			);
		},

		save: props => {

			return (
                <a href={props.attributes.link}
                    id={props.attributes.tabId}
                    className='nav-link quse__tab'
                    rel="noopener noreferrer"
                    aria-pressed="true"
                    aria-controls={props.attributes.linkName}
                    role='tab'
                >
                    {props.attributes.linkText}
                </a>
			);
		}
	}
);
