/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { SelectControl, TextControl, BaseControl, PanelBody, PanelRow } = wp.components;
const { InspectorControls, ColorPalette, InnerBlocks, RichText } = wp.blockEditor;
const { AlignmentToolbar, BlockControls, BlockAlignmentToolbar } = wp.editor;

/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

/**
 * Base Components
 */
import BackgroundOptions, { BackgroundOptionsAttributes, BaseColors, BackgroundOptionsClasses, BackgroundOptionsInlineStyles, ExperienceClasses } from '../base';
import { HeadingType } from '../base/components';

export default registerBlockType ("quse-blocks/quse-modal",
    {
        title: __("Modal"),
        description: __("A block that generates a modal"),
        category: "quse-block-layouts",
        icon: "align-full-width",
        attributes: {
            title: {
				type: "string",
				default: "Modal Title"
            },
            titleSize: {
                type: "string",
                default: 'heading-three'
            },
            titleColor: {
                type: 'string'
            },
			text: {
                type: 'array',
                source: 'children',
                selector: '.modal__body',
			},
            textColor: {
                type: 'string'
            },
            alignment: {
                type: 'string'
            },
            ...BackgroundOptionsAttributes,
        },
        
        edit: props => {

            const ALLOWED_BLOCKS = [ 'quse-blocks/quse-link' ];
            
            const ColorControl = (props) => (
                <BaseControl
                    label={props.label}
                >
                    <ColorPalette
                        colors={BaseColors}
                        value={props.value}
                        onChange={props.onChange}
                    />
                </BaseControl>
            );

            const onChangeTitle = value => { props.setAttributes( { title: value } ) };
            const onChangeTitleSize = value => props.setAttributes({titleSize: value});
            const onChangeText = value => { props.setAttributes( { text: value } ) };
            const onChangeTitleColor = value => { props.setAttributes( { titleColor: value } ) };
            const onChangeTextColor = value => { props.setAttributes( { textColor: value } ) };
            const onChangeAlignment = value => props.setAttributes({alignment: value});
            const onChangeBlockSize = value => props.setAttributes({blockAlignment: value});

            return (
                <div className={ classnames(
                    'quse-block',
                    'modal',
                    'quse-modal',
                    'fade',
                )} id={props.attributes.blockId}>
                    <div className="badge-block-name">Modal Layout</div>
                    <BlockControls>
                        <BlockAlignmentToolbar
                            value={ props.attributes.blockAlignment }
                            onChange={ onChangeBlockSize }
                        />
                        <AlignmentToolbar
                            value={ props.attributes.alignment }
                            onChange={ onChangeAlignment }
                        />
                    </BlockControls>
                    <InspectorControls key="inspector">
                        { BackgroundOptions( props ) }
                        <PanelBody
                            title={ __( 'Modal Title' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Size' ) }
                                    value={props.attributes.titleSize}
                                    options={HeadingType}
                                    onChange={onChangeTitleSize}
                                />
                            </PanelRow>
                            <PanelRow>
                                <ColorControl
                                    label={'Color'}
                                    onChange={onChangeTitleColor}
                                    value={props.attributes.titleColor}
                                />
                            </PanelRow>
                        </PanelBody>
                        <PanelBody title={ __( 'Modal Content' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <ColorControl
                                    label={'Choose Content Color'}
                                    onChange={onChangeTextColor}
                                    value={props.attributes.textColor}
                                />
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className={ classnames(
                            props.className,
                            'modal-content',
                            'modal__inner',
                            ...BackgroundOptionsClasses( props ),
                            ExperienceClasses(props),
                        ) } 
                        style={ {
                            ...BackgroundOptionsInlineStyles( props ),
                        } }>
                            <div className="modal-header modal__header">
                                <div className={ classnames(
                                        'modal-title',
                                        'modal__header__title',
                                        props.attributes.titleSize,
                                    )}
                                    style={{
                                    color: props.attributes.titleColor
                                }}>
                                    <RichText
                                        value={ props.attributes.title } // Any existing content, either from the database or an attribute default
                                        allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                                        onChange={onChangeTitle} // Store updated content as a block attribute
                                    />
                                </div>
                            </div>
                            <div className="modal-body modal__body" style={{
                                'text-align': props.attributes.alignment,
                                color: props.attributes.textColor
                            }}>
                                <RichText
                                    value={ props.attributes.text } // Any existing content, either from the database or an attribute default
                                    allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                                    onChange={onChangeText} // Store updated content as a block attribute
                                    placeholder={ __( 'Enter your text here...' ) } // Display this text before any content has been added by the user
                                />
                            </div>
                            <div class="modal-footer">
                                <InnerBlocks allowedBlocks={ ALLOWED_BLOCKS } />
                            </div>
                        </div>
                    </div>
                </div>
			);
		},

		save: props => {
            return (
                <div className={ classnames(
                    'quse-block',
                    'modal',
                    'quse-modal',
                    'fade',
                )} id={props.attributes.blockId}  tabindex="-1" role="dialog" aria-labelledby="quseModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content modal__inner" style={ {
                            ...BackgroundOptionsInlineStyles( props ),
                        } }>
                            <div className="modal-header modal__header">
                                <div className={ classnames(
                                    'modal-title',
                                    'modal__header__title',
                                    props.attributes.titleSize,
                                )} style={{color: props.attributes.titleColor}}>
                                    {props.attributes.title}
                                </div>

                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div className="modal-body modal__body" style={{
                                'text-align': props.attributes.alignment,
                                color: props.attributes.textColor
                            }}>
                                {props.attributes.text}
                            </div>

                            <div class="modal-footer modal__footer">
                                <InnerBlocks.Content />
                            </div>
                        </div>
                    </div>
                </div>
            )
        },

	}
);
