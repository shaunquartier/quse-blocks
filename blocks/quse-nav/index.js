/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { PanelBody, PanelRow, SelectControl } = wp.components;
const { InspectorControls, InnerBlocks } = wp.blockEditor;
const { BlockControls, BlockAlignmentToolbar } = wp.editor;

/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

/**
 * Base Components
 */
import BackgroundOptions, { BackgroundOptionsAttributes, BaseColors, BackgroundOptionsClasses, BackgroundOptionsInlineStyles, BackgroundOptionsVideoOutput, ExperienceClasses } from '../base';
import { FlexOptions } from '../base/components';

const navStyles = [
    { label: 'Horizontal Navigation', value: 'nav navigation' },
    { label: 'Vertical Navigation', value: 'nav flex-column' },
];

const tabStyles = [
    { label: 'Tab Style', value: 'nav nav-tabs' },
    { label: 'Pill Style', value: 'nav nav-pills' },
];

const navFunction = [
    { label: 'Tabs', value: 'tabs' },
    { label: 'Navigation', value: 'navigation' },
];

export default registerBlockType ("quse-blocks/quse-nav",
    {
        title: __("Navigation Layout"),
        description: __("A block that generates a series of links in a navigation layout."),
        category: "quse-block-layouts",
        icon: "menu",
        attributes: {
            style: {
                type: 'string',
                default: 'nav navigation'
            },
            alignment: {
                type: 'string',
                default: 'flex-start'
            },
            role: {
                type: 'string'
            },
            tabId: {
                type: 'string'
            },
            function: {
                type: 'string',
                default: 'navigation'
            },
            ...BackgroundOptionsAttributes,
        },
        
        edit: props => {
            const NAV_ALLOWED_BLOCKS = [ 'quse-blocks/quse-link' ];
            const onChangeStyle = value => { props.setAttributes( { style: value } ) };
            const onChangeAlignment = value => { props.setAttributes( { alignment: value } ) };
            const onChangeFunction = value => { props.setAttributes( { function: value } ) };
            const onChangeBlockSize = value => props.setAttributes({blockAlignment: value});

            if (props.attributes.function == 'tabs') {
                props.setAttributes( { role: 'tablist' } );
                props.setAttributes( { tabId: 'quseTab' } );

            } else {
                props.setAttributes( { role: 'navigation' } );
                props.setAttributes( { tabId: 'quseNav' } );
            }

            return (
                <section className={ classnames(
                    props.attributes.style,
                    'quse-block',
                    'nav',
                    ...BackgroundOptionsClasses( props ),
                    ExperienceClasses(props),
                )} 
                    style={{ 
                        "justify-content": props.attributes.alignment,
                        ...BackgroundOptionsInlineStyles( props )
                    }} 
                    id={props.attributes.tabId}
                    role={props.attributes.role}
                >
                    <div className="badge-block-name">Navigation Layout</div>
                    <BlockControls>
                        <BlockAlignmentToolbar
                            value={ props.attributes.blockAlignment }
                            onChange={ onChangeBlockSize }
                        />
                    </BlockControls>
                    <InspectorControls key="inspector">
                        { BackgroundOptions( props ) }

                            {props.attributes.function == 'navigation' ? (
                                <PanelBody
                                    title={ __( 'Navigation Settings' ) }
                                    className="quse-background-options"
                                    initialOpen={ false }
                                >
                                    <PanelRow>
                                        <SelectControl
                                            label={ __( 'Style: ' ) }
                                            value={props.attributes.style}
                                            options={navStyles}
                                            onChange={onChangeStyle}
                                        />
                                    </PanelRow>
                                    <PanelRow>
                                        <SelectControl
                                            label={ __( 'Alignment: ' ) }
                                            value={props.attributes.alignment}
                                            options={FlexOptions}
                                            onChange={onChangeAlignment}
                                        />
                                    </PanelRow>
                                </PanelBody>
                            ) : (
                                <PanelBody
                                    title={ __( 'Tab Link Settings' ) }
                                    className="quse-background-options"
                                    initialOpen={ false }
                                >
                                    <PanelRow>
                                        <SelectControl
                                            label={ __( 'Style: ' ) }
                                            value={props.attributes.style}
                                            options={tabStyles}
                                            onChange={onChangeStyle}
                                        />
                                    </PanelRow>
                                </PanelBody>
                            )}

                    </InspectorControls>

                    <InnerBlocks allowedBlocks={ NAV_ALLOWED_BLOCKS } />
                </section>
			)
		},

		save: props => {
            return (
                <section className={ classnames(
                    props.attributes.style,
                    'quse-block',
                    ...BackgroundOptionsClasses( props ),
                    ExperienceClasses(props),
                )} 
                    style={{ 
                        "justify-content": props.attributes.alignment,
                        ...BackgroundOptionsInlineStyles( props )
                    }}  
                    id={props.attributes.tabId}
                    role={props.attributes.role}
                >
                    <InnerBlocks.Content />
                </section>
            )
        },

	}
);