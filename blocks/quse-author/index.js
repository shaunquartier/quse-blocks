/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement, Fragment} = wp.element;
const { InspectorControls, MediaUpload, RichText } = wp.blockEditor;
const { BaseControl, PanelBody, PanelRow, Dashicon, SelectControl, TextControl, ToggleControl, Button } = wp.components;

/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

/**
 * Base Components
 */
import { BaseColors } from '../base';
import { ImageSizeOptions, ImagePositionOptions } from '../base/components';

export default registerBlockType ("quse-blocks/quse-author",
    {
        title: __("Author Block"),
        description: __("A block that will insert an Author"),
        category: "quse-block-components",
        icon: "id",
        attributes: {
            authorName: {
                type: 'string',
            },
            authorDescription: {
                type: 'string',
            },
            authorimgURL: {
				type: "string",
				selector: ".author__image"
			},
			authorimgID: {
				type: "number"
			},
			authorimgAlt: {
				type: "string",
				selector: ".author__image"
            },
            imageSize: {
                type: 'string',
                default: 'cover',
            },
            imagePosition: {
                type: 'string',
                default: 'center',
            },
        },

        edit: props => {

            const onSelectImage = img => {
				props.setAttributes({
					authorimgID: img.id,
					authorimgURL: img.url,
					authorimgAlt: img.alt
				});
			};
			const onRemoveImage = () => {
				props.setAttributes({
					authorimgID: null,
					authorimgURL: null,
				    authorimgAlt: null
				});
            };

            const onChangeAuthorName = value => props.setAttributes({authorName: value});
            const onChangeAuthorDesc = value => { props.setAttributes( { authorDescription: value } ) };
            const onChangeImageSize = value => props.setAttributes({imageSize: value});
            const onChangeImagePosition = value => props.setAttributes({imagePosition: value});

            return (
                <div>
                    <div className="badge-block-name">Author Block</div>
                    <InspectorControls key="inspector">
                        <PanelBody
                            title={ __( 'Author Options' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            { !props.attributes.authorimgID ? (
                                <div>
                                    <PanelRow>
                                        <div className="media-upload-wrapper">
                                            Author Image <br />
                                            <MediaUpload
                                                onSelect={onSelectImage}
                                                type="image"
                                                className="author__image"
                                                value={props.attributes.authorimgID}
                                                render={({ open }) => (
                                                    <Button className="button button-large quse-media-button" onClick={ open }>
                                                        <Dashicon icon="format-image" /> { __( 'Upload Image' ) }
                                                    </Button>
                                                )}
                                            />
                                        </div>
                                    </PanelRow>
                                    <PanelRow>
                                        <SelectControl
                                            label={ __( 'Image Size: ' ) }
                                            value={props.attributes.imageSize}
                                            options={ImageSizeOptions}
                                            onChange={onChangeImageSize}
                                        />
                                    </PanelRow>
                                    <PanelRow>
                                        <SelectControl
                                            label={ __( 'Image Position: ' ) }
                                            value={props.attributes.imagePosition}
                                            options={ImagePositionOptions}
                                            onChange={onChangeImagePosition}
                                        />
                                    </PanelRow>
                                </div>
                            ) : (
                            <div>
                                <PanelRow>
                                    <img
                                        src={ props.attributes.authorimgURL }
                                        alt={ props.attributes.authorimgAlt }
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <div className="media-upload-wrapper">
                                        <p>
                                            <Button
                                                className="remove-image button button-large quse-media-button"
                                                onClick={ onRemoveImage }
                                            >
                                                <Dashicon icon="no-alt" /> { __( 'Remove Image' ) }
                                            </Button>
                                        </p>
                                    </div>
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Size: ' ) }
                                        value={props.attributes.imageSize}
                                        options={ImageSizeOptions}
                                        onChange={onChangeImageSize}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Position: ' ) }
                                        value={props.attributes.imagePosition}
                                        options={ImagePositionOptions}
                                        onChange={onChangeImagePosition}
                                    />
                                </PanelRow>
                            </div>
                            )}
                        </PanelBody>
                    </InspectorControls>
                    
                    <div className="author container-wrapper">
                        <div className="author__box text-wrapper">
                    { ! props.attributes.authorimgID ? (
                            <div className="author__top__image">
                                <MediaUpload
                                    onSelect={onSelectImage}
                                    type="image"
                                    className="author__image"
                                    value={props.attributes.authorimgID}
                                    render={ ( { open } ) => (
                                        <Button className="button button-large quse-media-button" onClick={ open }>
                                            <Dashicon icon="format-image" /> { __( 'Upload Image' ) }
                                        </Button>
                                    ) }
                                />
                            </div>
                        ) : (
                            <div className="author__top">
                                <img className="author__top__image" src={props.attributes.authorimgURL} alt={props.attributes.authorimgAlt} />
                            </div>
                        )}
                        <div className="author__body">
                            <div className="author__body__content">
                                <div className="author__body__content__name">
                                    <RichText
                                        value={ props.attributes.authorName } // Any existing content, either from the database or an attribute default
                                        allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                                        onChange={onChangeAuthorName} // Store updated content as a block attribute
                                        placeholder={"Enter Author's Name"}
                                    />
                                </div>
                                    <div className="author__body__content__description">
                                        <RichText
                                            value={ props.attributes.authorDescription } // Any existing content, either from the database or an attribute default
                                            allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                                            onChange={onChangeAuthorDesc} // Store updated content as a block attribute
                                            placeholder={"Enter a description for the author..."}
                                        />
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            );
        },

		save: props => {
			return (
                <div className="author container-wrapper">
                    <div className="author__box text-wrapper">
                        {props.attributes.authorimgURL &&
                            <div className="author__top">
                                <img className="author__top__image" src={ props.attributes.authorimgURL } alt={ props.attributes.authorimgAlt } />
                            </div>
                        }
                        <div className="author__body">
                            <div className="author__body__content">
                                <div className="author__body__content__name">
                                    {props.attributes.authorName}
                                </div>
                                {props.attributes.authorDescription &&
                                    <div className="author__body__content__description">
                                        {props.attributes.authorDescription}
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            );
        }

    }
);
