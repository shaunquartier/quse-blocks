/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { SelectControl, TextControl, Dashicon, BaseControl, PanelBody, PanelRow, Button, RangeControl } = wp.components;
const { InspectorControls, ColorPalette, InnerBlocks, MediaUpload, RichText } = wp.blockEditor;
const { AlignmentToolbar, BlockControls } = wp.editor;

/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

/**
 * Base Components
 */
import { BaseColors } from '../base';
import { HeadingType, ImageSizeOptions, ImagePositionOptions, LinkOptions } from '../base/components';

export default registerBlockType ("quse-blocks/quse-card",
    {
        title: __("Card"),
        description: __("A block that generates a card"),
        category: "quse-block-components",
        icon: "analytics",
        parent: ['quse-blocks/quse-group'],
        attributes: {
            title: {
				type: "string",
                default: 'Card Title',
            },
            titleSize: {
                type: "string",
                default: 'heading-three'
            },
            titleColor: {
                type: 'string'
            },
			text: {
                type: 'string',
			},
            textColor: {
                type: 'string'
            },
            cardimgURL: {
				type: "string",
			},
			cardimgID: {
				type: "number"
			},
			cardimgAlt: {
				type: "string",
				source: "attribute",
				attribute: "alt",
				selector: "img"
            },
            cardImageSize: {
                type: 'string',
                default: 'cover',
            },
            cardImagePosition: {
                type: 'string',
                default: 'center',
            },
            alignment: {
                type: 'string'
            },
            cardStyle: {
                type: 'string',
                default: 'default'
            },
            cardGalleryLink: {
                type: 'string',
            },
            cardGalleryLinkTarget: {
                type: 'string',
                default: '_self'
            },
            cardOverlayColor: {
                type: 'string',
            },
            cardOverlayOpacity: {
                type: 'integer',
            },
            cardImageHeight: {
                type: 'string',
                default: null,
            },
        },
        
        edit: props => {
            
            const ALLOWED_BLOCKS = [ 'quse-blocks/quse-link' ];

            const cardStyleOptions = [
                { label: 'Default (Image on top)', value: 'default' },
                { label: 'Gallery (Image and title)', value: 'gallery' },
            ];
            
            const ColorControl = (props) => (
                <BaseControl
                    label={props.label}
                >
                    <ColorPalette
                        colors={BaseColors}
                        value={props.value}
                        onChange={props.onChange}
                    />
                </BaseControl>
            );
    
            const onSelectImage = img =>
				props.setAttributes({
					cardimgID: img.id,
					cardimgURL: img.url,
					cardimgAlt: img.alt
				});
			
			const onRemoveImage = () =>
				props.setAttributes({
					cardimgID: null,
					cardimgURL: null,
					cardimgAlt: null
                });
                
            const onRemoveImageHeight = () => props.setAttributes({ cardImageHeight: null });
            const onSetImageHeight = () => props.setAttributes({ cardImageHeight: '200' });

            const onChangeAlignment = value => props.setAttributes({alignment: value});
            const onChangeStyle = value => props.setAttributes({cardStyle: value});
            const onChangeTitle = value => { props.setAttributes( { title: value } ) };
            const onChangeTitleSize = value => props.setAttributes({titleSize: value});
            const onChangeText = value => { props.setAttributes( { text: value } ) };
            const onChangeTitleColor = value => { props.setAttributes( { titleColor: value } ) };
            const onChangeTextColor = value => { props.setAttributes( { textColor: value } ) };
            const onChangeImageSize = value => props.setAttributes({cardImageSize: value});
            const onChangeImagePosition = value => props.setAttributes({cardImagePosition: value});
            const onChangeCardLink = value => props.setAttributes({cardGalleryLink: value});
            const onChangeCardLinkTarget = value => props.setAttributes({cardGalleryLinkTarget: value});
            const setCardOverlayColor = value => props.setAttributes( { cardOverlayColor: value } );
            const onChangeCardOpacity = value => props.setAttributes( { cardOverlayOpacity: value } );
            const onChangeCardImageHeight = value => props.setAttributes({cardImageHeight: value});

            return (
                <div className="card__box">
                    <BlockControls>
                        <AlignmentToolbar
                            value={ props.attributes.alignment }
                            onChange={ onChangeAlignment }
                        />
                    </BlockControls>
                    <InspectorControls key="inspector">
                        <PanelBody
                            title={ __( 'Card Style & Options' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Card Style: ' ) }
                                    value={props.attributes.cardStyle}
                                    options={cardStyleOptions}
                                    onChange={onChangeStyle}
                                />
                            </PanelRow>
                        </PanelBody>
                    { !props.attributes.cardimgID ? (
                        <PanelBody
                            title={ __( 'Card Image' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <div className="media-upload-wrapper">
                                    <MediaUpload
                                        onSelect={onSelectImage}
                                        type="image"
                                        className="card_image"
                                        value={props.attributes.cardimgID}
                                        render={ ( { open } ) => (
                                            <Button className="button button-large" onClick={ open }>
                                                <Dashicon icon="format-image" /> { __( 'Upload Image' ) }
                                            </Button>
                                        ) }
                                    />
                                </div>
                            </PanelRow>
                        </PanelBody>
                    ) : (
                        <PanelBody
                            title={ __( 'Card Image' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <img src={ props.attributes.cardimgURL } alt={ props.attributes.cardimgAlt } />
                            </PanelRow>
                            <PanelRow>
                                { props.attributes.cardImageHeight === null ? (
                                    <div className="media-button-wrapper panel--clear">
                                        <p>
                                        <Button
                                            className="set-image button button-large"
                                            onClick={ onRemoveImage }
                                        >
                                            { __( 'Remove Image' ) }
                                        </Button>
                                        </p>
                                        <p>Image Height: 100%</p>
                                        <p>
                                            <Button
                                                className="set-image button button-large"
                                                onClick={ onSetImageHeight }
                                            >
                                                { __( 'Set Image Height' ) }
                                            </Button>
                                        </p>
                                    </div>
                                ) : (
                                    <div className="media-button-wrapper panel--clear">
                                        <TextControl
                                            label={"Image Height: "}
                                            className={"small-box"}
                                            help={"Set Update the height above in (px) or set to 100% below"}
                                            value={props.attributes.cardImageHeight}
                                            onChange={onChangeCardImageHeight}
                                        />
                                            <Button
                                                className="remove-image button button-large"
                                                onClick={ onRemoveImageHeight }
                                            >
                                                { __( 'Set to 100%' ) }
                                            </Button>
                                    </div>
                                )}
                            </PanelRow>
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Image Size: ' ) }
                                    value={props.attributes.cardImageSize}
                                    options={ImageSizeOptions}
                                    onChange={onChangeImageSize}
                                />
                            </PanelRow>
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Image Position: ' ) }
                                    value={props.attributes.cardImagePosition}
                                    options={ImagePositionOptions}
                                    onChange={onChangeImagePosition}
                                />
                            </PanelRow>
                            <PanelRow>
                                <ColorControl
                                    label={'Overlay Color'}
                                    onChange={setCardOverlayColor}
                                    value={props.attributes.cardOverlayColor}
                                />
                            </PanelRow>
                            <PanelRow>
                                <RangeControl
                                    label="Overlay Opacity"
                                    value={ props.attributes.cardOverlayOpacity }
                                    help={"Must be between 0 - 1"}
                                    onChange={ onChangeCardOpacity }
                                    min={ 0 }
                                    max={ 1 }
                                    step={ 0.05 }
                                />
                            </PanelRow>
                        </PanelBody>
                    ) }
                        <PanelBody
                            title={ __( 'Card Content' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Title Size' ) }
                                    value={props.attributes.titleSize}
                                    options={HeadingType}
                                    onChange={onChangeTitleSize}
                                />
                            </PanelRow>
                            <PanelRow>
                                <ColorControl
                                    label={'Choose Title Color'}
                                    onChange={onChangeTitleColor}
                                    value={props.attributes.titleColor}
                                />
                            </PanelRow>
                            { props.attributes.cardStyle == 'default' ? (
                            <div>
                                <PanelRow>
                                    <ColorControl
                                        label={'Choose Content Color'}
                                        onChange={onChangeTextColor}
                                        value={props.attributes.textColor}
                                    />
                                </PanelRow>
                            </div>
                            ) : (
                                <div>
                                    <PanelRow>
                                        <TextControl
                                            label={'Card Link'}
                                            value={props.attributes.cardGalleryLink}
                                            onChange={onChangeCardLink}
                                        />
                                    </PanelRow>
                                    <PanelRow>
                                        <SelectControl
                                            label={ __( 'Card Link Target' ) }
                                            value={props.attributes.cardGalleryLinkTarget}
                                            options={LinkOptions}
                                            onChange={onChangeCardLinkTarget}
                                        />
                                    </PanelRow>
                                    
                                </div>
                            )}
                        </PanelBody>
                    </InspectorControls>
                    { props.attributes.cardStyle == 'default' ? (
                    <div className="card">
                        <div className="badge-block-name">Card</div>
                        { ! props.attributes.cardimgID ? ( // No image, Default style
                            <div className="card-image-top" style={{
                                height: null !== props.attributes.cardImageHeight ? props.attributes.cardImageHeight + 'px' : null,
                            }}>
                                <MediaUpload
                                    onSelect={onSelectImage}
                                    type="image"
                                    className="card_image"
                                    value={props.attributes.cardimgID}
                                    render={ ( { open } ) => (
                                        <Button className="button button-large quse-media-button quse-media-button" onClick={ open }>
                                            <Dashicon icon="format-image" /> { __( 'Upload Image' ) }
                                        </Button>
                                    ) }
                                />
                            </div>

                            ) : ( // Image exists, Default style
                            <div className="card-image-top" style={{
                                height: null !== props.attributes.cardImageHeight ? props.attributes.cardImageHeight + 'px' : null,
                                backgroundImage: "url(" + props.attributes.cardimgURL + ")",
                                backgroundSize: props.attributes.cardImageSize,
                                backgroundPosition: props.attributes.cardImagePosition,
                            }}>
                                <div className="background__overlay" style={{
                                    backgroundColor: props.attributes.cardOverlayColor,
                                    opacity: props.attributes.cardOverlayOpacity
                                }}></div>

                                <Button
                                    className="remove-image button button-large quse-media-button quse-media-button--overlay"
                                    onClick={ onRemoveImage }
                                >
                                    <Dashicon icon="no-alt" /> { __( 'Remove Image' ) }
                                </Button>
                            </div>
                            )}
                        <div className={ classnames(
                            "card-body",
                            "card-" + props.attributes.alignment,
                        )} style={{
                            'text-align': props.attributes.alignment
                        }}>
                            <div className={ classnames(
                                    'card-title',
                                    props.attributes.titleSize,
                                )}
                                style={{
                                    color: props.attributes.titleColor
                            }}>
                                <RichText
                                    value={ props.attributes.title } // Any existing content, either from the database or an attribute default
                                    allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                                    onChange={onChangeTitle} // Store updated content as a block attribute
                                    placeholder={ __( 'Enter Card Title...' ) } // Display this text before any content has been added by the user
                                />
                            </div>
                            <div className="card-text" style={{
                                color: props.attributes.textColor
                            }}>
                                <RichText
                                    value={ props.attributes.text } // Any existing content, either from the database or an attribute default
                                    allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                                    onChange={onChangeText} // Store updated content as a block attribute
                                    placeholder={ __( 'Enter text here...' ) } // Display this text before any content has been added by the user
                                />
                            </div>
                            <InnerBlocks allowedBlocks={ ALLOWED_BLOCKS } />
                        </div>
                    </div>
                    ) : (
                        <a className='card card--gallery'
                            rel="noopener noreferrer"
                            aria-pressed="true"
                            target={props.attributes.cardGalleryLinkTarget}
                        >
                            <div className="badge-block-name">Card</div>
                            <div className="card-image-top" style={{
                                height: null !== props.attributes.cardImageHeight ? props.attributes.cardImageHeight + 'px' : null,
                            }}>
                                <img src={ props.attributes.cardimgURL } alt={ props.attributes.cardimgAlt } />
                                <div className="background__overlay" style={{
                                    backgroundColor: props.attributes.cardOverlayColor,
                                    opacity: props.attributes.cardOverlayOpacity
                                }}></div>
                                <div className={ classnames(
                                        'card-title',
                                        props.attributes.titleSize,
                                    )} 
                                    style={{
                                        color: props.attributes.titleColor
                                }}>
                                    <RichText
                                        value={ props.attributes.title } // Any existing content, either from the database or an attribute default
                                        allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                                        onChange={onChangeTitle} // Store updated content as a block attribute
                                        placeholder={ __( 'Enter Card Title...' ) } // Display this text before any content has been added by the user
                                    />
                                </div>
                            </div>
                        </a>
                    )}
                </div>
			);
		},

		save: props => {
			return (
                <div className="card__box">
                    { props.attributes.cardStyle == 'default' ? (
                        <div className="card">
                            {props.attributes.cardimgURL &&
                                <div className="card-image-top" style={{
                                    height: null !== props.attributes.cardImageHeight ? props.attributes.cardImageHeight + 'px' : null,
                                    backgroundImage: "url(" + props.attributes.cardimgURL + ")",
                                    backgroundSize: props.attributes.cardImageSize,
                                    backgroundPosition: props.attributes.cardImagePosition,
                                }}>
                                    <div className="background__overlay" style={{
                                        backgroundColor: props.attributes.cardOverlayColor,
                                        opacity: props.attributes.cardOverlayOpacity
                                    }}></div>
                                </div>
                            }
                            {(props.attributes.title || props.attributes.text) &&
                            <div className="card-body" style={{
                                'text-align': props.attributes.alignment
                            }}>
                                <div className={ classnames(
                                    'card-title',
                                    props.attributes.titleSize,
                                )} 
                                style={{
                                    color: props.attributes.titleColor
                                }}>
                                    {props.attributes.title}
                                </div>
                                <div className="card-text" style={{
                                    color: props.attributes.textColor
                                }}>
                                    {props.attributes.text}
                                </div>
                                <InnerBlocks.Content />
                            </div>
                            }
                        </div>
                    ) : (
                        <a className='card card--gallery'
                            href={props.attributes.cardGalleryLink}
                            target={props.attributes.cardGalleryLinkTarget}
                            rel="noopener noreferrer"
                            aria-pressed="true"
                        >
                            {props.attributes.cardimgURL &&
                                <div className="card-image-top" style={{
                                    height: null !== props.attributes.cardImageHeight ? props.attributes.cardImageHeight + 'px' : null,
                                }}>
                                    <img src={props.attributes.cardimgURL} alt={props.attributes.cardimgAlt} />
                                    <div className="background__overlay" style={{
                                        backgroundColor: props.attributes.cardOverlayColor,
                                        opacity: props.attributes.cardOverlayOpacity
                                    }}></div>
                                    <div className={ classnames(
                                    'card-title',
                                    props.attributes.titleSize,
                                    )} 
                                        style={{
                                        color: props.attributes.titleColor
                                    }}>
                                        {props.attributes.title}
                                    </div>
                                </div>
                            }
                        </a>
                    )}
                </div>
			);
		}
	}
);
