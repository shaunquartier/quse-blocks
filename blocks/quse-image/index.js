/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { TextControl, BaseControl, PanelBody, PanelRow, SelectControl, Dashicon, Button } = wp.components;
const { InspectorControls, ColorPalette, InnerBlocks, MediaUpload, RichText } = wp.blockEditor;
const { AlignmentToolbar, BlockControls, BlockAlignmentToolbar } = wp.editor;

/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

/**
 * Base Components
 */
import BackgroundOptions, { BackgroundOptionsAttributes, BaseColors, BackgroundOptionsClasses, BackgroundOptionsInlineStyles, BackgroundOptionsVideoOutput, ExperienceClasses } from '../base';
import { HeadingType, ImageSizeOptions, ImagePositionOptions } from '../base/components';

const imageAttachmentOptions = [
    { label: 'Scroll', value: 'scroll' },
    { label: 'Fixed', value: 'fixed' },
    { label: 'Inherit', value: 'inherit' },
    { label: 'Initial', value: 'initial' },
];

const imageRepeatOptions = [
    { label: 'Repeat', value: 'repeat' },
    { label: 'No Repeat', value: 'no-repeat' },
    { label: 'Repeat X', value: 'repeat-x' },
    { label: 'Repeat Y', value: 'repeat-y' },
];

export default registerBlockType ("quse-blocks/quse-image",
    {
        title: __("Image Display"),
        description: __("A block that displays an image layout for your page."),
        category: "quse-block-layouts",
        icon: "format-image",
        attributes: {
            imageURL: {
				type: "string",
				selector: ".poster__image"
			},
			imageID: {
				type: "number"
			},
			imageAlt: {
				type: "string",
				selector: ".poster__image"
            },
            imageSize: {
                type: 'string',
                default: 'cover',
            },
            imagePosition: {
                type: 'string',
                default: 'center',
            },
            imageAttachment: {
                type: 'string',
            },
            imageRepeat: {
                type: 'string',
            },
            imageLayoutHeight: {
                type: 'string',
                default: '500',
            },
            ...BackgroundOptionsAttributes,
        },
        
        edit: props => {

            const onSelectImage = img => {
				props.setAttributes({
					imageID: img.id,
					imageURL: img.url,
					imageAlt: img.alt
				});
			};
			const onRemoveImage = () => {
				props.setAttributes({
					imageID: null,
					imageURL: null,
					imageAlt: null
				});
            };

            const onChangeImageSize = value => props.setAttributes({imageSize: value});
            const onChangeImagePosition = value => props.setAttributes({imagePosition: value});
            const onChangeImageAttachment = value => props.setAttributes({imageAttachment: value});
            const onChangeImageRepeat = value => props.setAttributes({imageRepeat: value});
            const onChangeBlockSize = value => props.setAttributes({blockAlignment: value});
            const onChangeImageHeight = value => props.setAttributes({imageLayoutHeight: value});
        
            return (
                <div>
                    <div className="badge-block-name">Image Layout</div>
                    <BlockControls>
                        <BlockAlignmentToolbar
                            value={ props.attributes.blockAlignment }
                            onChange={ onChangeBlockSize }
                        />
                    </BlockControls>
                    <InspectorControls key="inspector">
                        { BackgroundOptions( props ) }
                        { !props.attributes.imageID ? (
                                <PanelBody
                                title={ __( 'Image' ) }
                                className="quse-background-options"
                                initialOpen={ false }
                            >
                                <PanelRow>
                                    <div className="media-upload-wrapper">
                                        <MediaUpload
                                            onSelect={onSelectImage}
                                            type="image"
                                            className="image"
                                            value={props.attributes.imageID}
                                            render={({ open }) => (
                                                <Button className="button button-large" onClick={ open }>
                                                    <Dashicon icon="format-image" /> { __( 'Upload Image' ) }
                                                </Button>
                                            )}
                                        />
                                    </div>
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Size: ' ) }
                                        value={props.attributes.imageSize}
                                        options={ImageSizeOptions}
                                        onChange={onChangeImageSize}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Position: ' ) }
                                        value={props.attributes.imagePosition}
                                        options={ImagePositionOptions}
                                        onChange={onChangeImagePosition}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Attachment: ' ) }
                                        value={props.attributes.imageAttachment}
                                        options={imageAttachmentOptions}
                                        onChange={onChangeImageAttachment}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Repeat: ' ) }
                                        value={props.attributes.imageRepeat}
                                        options={imageRepeatOptions}
                                        onChange={onChangeImageRepeat}
                                    />
                                </PanelRow>
                            </PanelBody>
                        ) : (
                            <PanelBody
                                title={ __( 'Image' ) }
                                className="quse-background-options"
                                initialOpen={ false }
                            >
                                <PanelRow>
                                        <img
                                            src={ props.attributes.imageURL }
                                            alt={ props.attributes.imageAlt }
                                        />
                                </PanelRow>
                                <PanelRow>
                                    <TextControl
                                        label={"Image Height: "}
                                        className={"small-box"}
                                        help={"Set Update the height above in (px) or set to 100% below"}
                                        value={props.attributes.imageLayoutHeight}
                                        onChange={onChangeImageHeight}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Size: ' ) }
                                        value={props.attributes.imageSize}
                                        options={ImageSizeOptions}
                                        onChange={onChangeImageSize}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Position: ' ) }
                                        value={props.attributes.imagePosition}
                                        options={ImagePositionOptions}
                                        onChange={onChangeImagePosition}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Attachment: ' ) }
                                        value={props.attributes.imageAttachment}
                                        options={imageAttachmentOptions}
                                        onChange={onChangeImageAttachment}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={ __( 'Image Repeat: ' ) }
                                        value={props.attributes.imageRepeat}
                                        options={imageRepeatOptions}
                                        onChange={onChangeImageRepeat}
                                    />
                                </PanelRow>
                            </PanelBody>
                        )}
                    </InspectorControls>
                    <section className={ classnames(
                        'quse-block',
                        'image__layout',
                        ...BackgroundOptionsClasses( props ),
                        ExperienceClasses(props),
                    ) } style={ {
                        ...BackgroundOptionsInlineStyles( props ),
                    } }>
                        <div className="background__overlay" style={{
                            backgroundColor: props.attributes.overlayColor,
                            opacity: props.attributes.overlayOpacity
                        }}></div>

                        <div className={ classnames(
                            'image',
                            'd-block',
                            'w-100',
                        )} style={{
                            backgroundImage: "url(" + props.attributes.imageURL + ")",
                            backgroundSize: props.attributes.imageSize,
                            backgroundPosition: props.attributes.imagePosition,
                            backgroundAttachment: props.attributes.imageAttachment,
                            backgroundRepeat: props.attributes.imageRepeat,
                            height: props.attributes.imageLayoutHeight + 'px',
                        }}>
                            { ! props.attributes.imageID ? (
                            <MediaUpload
                                onSelect={onSelectImage}
                                type="image"
                                className="image"
                                value={props.attributes.imageID}
                                render={({ open }) => (
                                    <Button className="button button-large quse-media-button quse-media-button" onClick={ open }>
                                        <Dashicon icon="format-image" /> { __( 'Upload Image' ) }
                                    </Button>
                                )}
                            />
                        ) : (
                            <Button
                                className="remove-image button button-large quse-media-button quse-media-button--overlay"
                                onClick={ onRemoveImage }
                            >
                                <Dashicon icon="no-alt" /> { __( 'Remove Image' ) }
                            </Button>
                        )}
                        </div>
                    </section>
                </div>
                )
		},

		save: props => {
            return (
                <section className={ classnames(
                    'quse-block',
                    'image__layout',
                    ...BackgroundOptionsClasses( props ),
                    ExperienceClasses(props),
                ) } style={ {
                    ...BackgroundOptionsInlineStyles( props ),
                } }>
                    <div className="background__overlay" style={{
                        backgroundColor: props.attributes.overlayColor,
                        opacity: props.attributes.overlayOpacity
                    }}></div>

                    <div className={ classnames(
                        'image',
                        'd-block',
                        'w-100',
                    )} style={{
                        backgroundImage: "url(" + props.attributes.imageURL + ")",
                        backgroundSize: props.attributes.imageSize,
                        backgroundPosition: props.attributes.imagePosition,
                        backgroundAttachment: props.attributes.imageAttachment,
                        backgroundRepeat: props.attributes.imageRepeat,
                        height: props.attributes.imageLayoutHeight + 'px',
                    }}>
                    </div>
                </section>
            )
        }
	}
);
