/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {createElement} = wp.element;
const { TextControl, BaseControl, PanelBody, PanelRow } = wp.components;
const { InspectorControls, ColorPalette, RichText } = wp.blockEditor;
const { AlignmentToolbar, BlockControls, BlockAlignmentToolbar } = wp.editor;

/**
 * Other dependencies
 */
import classnames from 'classnames'; // Import NPM libraries here.

/**
 * Base Components
 */
import { BaseColors } from '../base';

export default registerBlockType ("quse-blocks/quse-tab-content",
    {
        title: __("Tab Content"),
        description: __("A block that generates a tab content zone. Choose how you want to display the content in the settings below."),
        category: "quse-block-components",
        parent: ['quse-blocks/quse-tab-item'],
        icon: "text",
        attributes: {
            textColor: {
                type: 'string'
            },
            content: {
                type: 'array',
                source: 'children',
                selector: '.content',
            },
            tabId: {
                type: 'string',
            },
        },
        
        edit: props => {

            const onChangeTextColor = value => { props.setAttributes( { textColor: value } ) };
            const onChangeContent = value => { props.setAttributes( { content: value } ) };
            const onChangeId = value => props.setAttributes( { blockId: value } );
            const onChangeTab = value => { props.setAttributes( { tabId: value } ) };

            const ColorControl = (props) => (
                <BaseControl
                    label={props.label}
                >
                    <ColorPalette
                        colors={BaseColors}
                        value={props.value}
                        onChange={props.onChange}
                    />
                </BaseControl>
            );

            return (
                <div>
                    <div className="badge-block-name">Tab Content {props.attributes.tabId && '| Linked to (Tab): ' + props.attributes.tabId}</div>
                    <InspectorControls key="inspector">
                        <PanelBody
                            title={ __( 'Tab Content Settings' ) }
                            className="quse-background-options"
                            initialOpen={ false }
                        >
                            <PanelRow>
                                <TextControl
                                    label={"Tab Name"}
                                    value={props.attributes.tabId}
                                    onChange={onChangeTab}
                                    help={"You can find the Tab Name by clicking on the Tab that you want to link. Open the 'Tab Link Settings' and copy the value for this field."}
                                />
                            </PanelRow>
                            <PanelRow>
                                <ColorControl
                                    label={'Text Color'}
                                    onChange={onChangeTextColor}
                                    value={props.attributes.textColor}
                                />
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>
                    <div className={ classnames(
                        'tab-content',
                        'tab-pane',
                        props.attributes.wrapperWidth,
                    )} style={{color: props.attributes.textColor}} id={props.attributes.tabId} role='tabpanel' aria-labelledby={ props.attributes.tabId + '-tab' }>
                        <RichText
                            tagName="div" // The tag here is the element output and editable in the admin
                            multiline="p"
                            value={ props.attributes.content } // Any existing content, either from the database or an attribute default
                            allowedFormats={ [ 'bold', 'italic' ] } // Allow the content to be made bold or italic, but do not allow other formatting options
                            onChange={onChangeContent} // Store updated content as a block attribute
                            placeholder={ __( 'Enter your content here.' ) } // Display this text before any content has been added by the user
                        />
                    </div>
                </div>
			);
		},

		save: props => {
			return (
                <div className={ classnames(
                    'tab-content',
                    'tab-pane',
                )} style={{color: props.attributes.textColor}} id={props.attributes.tabId} role='tabpanel' aria-labelledby={ props.attributes.tabId + '-tab' }>
                    { props.attributes.content }
                </div>
			);
		}
	}
);
