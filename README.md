# quse Blocks for Gutenberg

## What Is This?
Code from [Shaun Quartier](https://qusedevelopment.com) and his components for the new block-based "Gutenberg" editor for WordPress.


## How To Install/ Use/ Learn
This repo has one block:

 * "Card" - A card display with a title, content, image, and link.

 ### Use JSX-version  
 * Install
    * `npm install`
 * Start watcher to compile JS
    * `npm run dev`
